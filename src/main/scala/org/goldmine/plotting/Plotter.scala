package org.goldmine.plotting
import com.cibo.evilplot.plot._
import com.cibo.evilplot.plot.aesthetics.DefaultTheme._
import com.cibo.evilplot.numeric.Point


class Plotter {
  val data = Seq.tabulate(100) { i =>
    Point(i.toDouble, scala.util.Random.nextDouble())
  }
  ScatterPlot(data)
    .xAxis()
    .yAxis()
    .frame()
    .xLabel("x")
    .yLabel("y")
    .render()
}
