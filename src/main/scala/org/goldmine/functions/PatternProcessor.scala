package org.goldmine.functions{

import scala.collection.mutable._
import org.akkatrading.live.StrategyFSM

object PatternProcessor {

	type Pattern = StrategyFSM.Pattern
	type PrecisionType = StrategyFSM.PrecisionType

	  @inline
	def getPercentChange(from: PrecisionType, to: PrecisionType): PrecisionType = {
		if(from != 0 && from == from) {
      val x = ((to - from) / Math.abs(from)) * 100.00
      if (x == 0.0)
        0.000000001
      else
        x
    }
    else {
				0.0001
		}
	}

	@inline
	def storePatterns(averagePrices: Pattern,
	                  patterns: ArrayBuffer[Pattern],
	                  performances: ArrayBuffer[PrecisionType]) = {

		//println("Store patterns ----------------------- ")

		val startTimeMillis = System.currentTimeMillis()

	    val (p, f) = averagePrices.tail.sliding(61).map { v =>
	      val outcomeRange = v.slice(50, 60)
	      val currentPoint = v(30)

	      val avgOutcome = outcomeRange.sum / outcomeRange.length
	      val futureOutcome = getPercentChange(currentPoint, avgOutcome)


	      val fromValue = v.head
	      val pattern = v.tail.dropRight(30) map { x => getPercentChange(fromValue, x) }

	      (pattern, futureOutcome)
	    }.toArray.unzip

	    patterns.appendAll(p)
	    performances.appendAll(f)

		//println("End loop ----------------------- ")

		//println("performancesSum=" + performances.sum)

		// var sum = 0.0
		// patterns.foreach(pattern => {
		// 	sum += pattern.sum
		// })
		//println("patternsSum=" + sum)

		val totalTimeMillis = System.currentTimeMillis() - startTimeMillis
		println("Pattern storing took: " + totalTimeMillis / 1000.0)
	}


  @inline
	def buildTicks(averagePrices: Array[PrecisionType]) = {

		//println("Build ticks ----------------------- ")

		val startTime = System.currentTimeMillis()

		//println(averagePrices.mkString("\n"))

    val interval = 30
    val fromValue = averagePrices(averagePrices.length - interval - 1)
    val ticks = averagePrices.takeRight(interval).map(v => getPercentChange(fromValue, v))

		println("CurrentPattern took: " + (System.currentTimeMillis() - startTime) / 1000.0)

		ticks
	}

	@inline
	def matchPatterns(ticks: Pattern,
	                  patterns: ArrayBuffer[Pattern],
	                  performances: ArrayBuffer[PrecisionType]): Double = {

		//println("Match patterns ----------------------- ")

		val startTime = System.currentTimeMillis()
		var predictionAverage = 0.0
		
		val predictedOutcomes = new ArrayBuffer[PrecisionType]
		val matchingPatterns = new ArrayBuffer[Pattern]
		var patternFound: Boolean = false


		patterns foreach { pattern =>
      val prices = pattern.view.zip(ticks).map(v => 100 - Math.abs(getPercentChange(v._1, v._2)))
      val (up40, up50) = prices.splitAt(20)
      val ok = up40.forall(_ > 40) && up50.init.forall(_ > 50)
      if (ok && (up40.sum + up50.sum) / 30 > 80) {
        patternFound = true
        matchingPatterns.append(pattern)
      }
    }

		//println("howSimsCount=" + howSims.length)
		//println("howSimsSum=" + howSims.sum)
//		println("Pattern loop took: " + (System.currentTimeMillis() - startTime) / 1000.0)
		val start2Time = System.currentTimeMillis()

		val predictions = new ArrayBuffer[PrecisionType]
		if (patternFound) {

			matchingPatterns.foreach(matchingPattern => {
				val futurePoints = patterns.indexWhere(_ == matchingPattern)

				if (performances(futurePoints) > ticks(29)) {
					//pcolor = '# 24 bc00 '
					predictions.append(1.0)
				}
				else {
					//pcolor = '# d40000 '
					predictions.append(-1.0)
				}
			})

//			println("Pattern future loop took: " + (System.currentTimeMillis() - start2Time) / 1000.0)

			predictionAverage = predictions.sum / predictions.length

		}

//		println("Pattern patternRecognition took: " + (System.currentTimeMillis() - startTime) / 1000.0)
		predictionAverage
	}


}

}
	