package org.goldmine.functions


import org.goldmine.indicator.Factor
import org.goldmine.timeseries.Null

import scala.collection.mutable.ArrayBuffer

case class ADXResult(adx: Double, adxPrev: Double, plusDi: Double, plusDiPrev: Double, minusDi: Double, minusDiPrev: Double)

class ADXFunction(var H: Array[Double], var L: Array[Double], var C: Array[Double], var periodDi: Factor, var periodAdx: Factor) {

  val _adx = ArrayBuffer.fill(C.length)(0.0)
  val _dx = ArrayBuffer.fill(C.length)(0.0)
  val _diPlus = ArrayBuffer.fill(C.length)(0.0)
  val _diMinus = ArrayBuffer.fill(C.length)(0.0)
  val _dmPlus = ArrayBuffer.fill(C.length)(0.0)
  val _dmMinus = ArrayBuffer.fill(C.length)(0.0)
  val _tr = ArrayBuffer.fill(C.length)(0.0)

  def adx(idx: Int): ADXResult = {

    var i = 0
    if(C.length == 0) return ADXResult(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    while (i < C.length) {

      if (i == 0) {
        _dmPlus(i) = 0.00
        _dmMinus(i) = 0.00
      } else {
        _dmPlus(i) = if (H(i) - H(i - 1) > L(i - 1) - L(i)) math.max(H(i) - H(i - 1), 0.0) else 0.00
        _dmMinus(i) = if (L(i - 1) - L(i) > H(i) - H(i - 1)) math.max(L(i - 1) - L(i), 0.00) else 0.00
      }

      if (i == 0) {
        _tr(i) = 0.00
      } else {
        val tr_tmp = math.max(H(i) - L(i), math.abs(H(i) - C(i - 1)))
        _tr(i) = math.max(tr_tmp, math.abs(L(i) - C(i - 1)))
      }

      if (i < periodDi.value - 1) {

        _diPlus(i) = 0.00
        _diMinus(i) = 0.00

      } else {
        
        
        val maForDmPlusFunction = new MAFunction(_dmPlus.toArray, periodDi)
        val maForDmMinusFunction = new MAFunction(_dmMinus.toArray, periodDi)
        val maForTrFunction = new MAFunction(_tr.toArray, periodDi)

        val dmPlus_ma = maForDmPlusFunction.ma(i)
        val dmMinus_ma = maForDmMinusFunction.ma(i)
        val tr_ma = maForTrFunction.ma(i)

        val diPlus_i = if (tr_ma == 0) 0.00 else (dmPlus_ma / tr_ma) * 100.0
        val diMinus_i = if (tr_ma == 0) 0.00 else (dmMinus_ma / tr_ma) * 100.0

        _diPlus(i) = diPlus_i
        _diMinus(i) = diMinus_i

      }

      if (i < periodDi.value - 1) {
        _dx(i) = 0.00
      } else {
        val diPlus_i = _diPlus(i)
        val diMinus_i = _diMinus(i)
        val dx_i = if (diPlus_i + diMinus_i == 0) 0.0 else math.abs(diPlus_i - diMinus_i) / (diPlus_i + diMinus_i) * 100.0
        _dx(i) = dx_i
      }

      if (i < periodDi.value - 1 || i < periodAdx.value - 1) {
        _adx(i) = 0.00
      } else {
        val maFunction = new MAFunction(_dx.toArray, periodDi)
        _adx(i) = maFunction.ma(i)
      }

      i += 1
    }

    if(idx > 0 )
      {
        ADXResult(_adx(idx), _adx(idx-1), _diPlus(idx), _diPlus(idx-1), _diMinus(idx), _diMinus(idx-1))
      }
    else
    {
      ADXResult(_adx(idx),0.00, _diPlus(idx), 0.00, _diMinus(idx), 0.00)
    }
      
  }

}

