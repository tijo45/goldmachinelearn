package org.goldmine.functions


import scala.collection.mutable.ArrayBuffer

class DMFunction(var H: Array[Double], var L: Array[Double], var C: Array[Double]) {

  val _dmPlus = ArrayBuffer.fill(C.length)(0.0)
  val _dmMinus = ArrayBuffer.fill(C.length)(0.0)
  var alreadyComputed = false

  protected def computeSpot(i: Int): Unit = {
    if (i == 0) {

      _dmPlus(i) = 0.0
      _dmMinus(i) = 0.0

    } else {

      _dmPlus(i) = if(H(i) - H(i - 1)>L(i - 1) - L(i)) math.max(H(i) - H(i - 1), 0.0) else 0.0
      _dmMinus(i) = if(L(i - 1) - L(i)>H(i) - H(i - 1)) math.max(L(i - 1) - L(i), 0.0) else 0.0

    }
  }

  def dmPlus(idx: Int): Double = {
    var i = 0

    if (!alreadyComputed) {
      while (i < C.length) {
        computeSpot(i)
        i += 1
      }
    }

    alreadyComputed = true

    _dmPlus(idx)
  }

  def dmMinus(idx: Int): Double = {
    var i = 0

    if (!alreadyComputed) {
      while (i < C.length) {
        computeSpot(i)
        i += 1
      }
    }

    alreadyComputed = true

    _dmMinus(idx)
  }
}

