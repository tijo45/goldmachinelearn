package org.goldmine.functions


import scala.collection.mutable.ArrayBuffer

class TRFunction(var H: Array[Double], var L: Array[Double], var C: Array[Double]) {

  val _tr = ArrayBuffer.fill(C.length)(0.0)

  protected def computeSpot(i: Int): Unit = {
    if (i == 0) {

      _tr(i) = 0.0

    } else {
      
      val tr_tmp = math.max(H(i) - L(i), math.abs(H(i) - C(i - 1)))
      _tr(i) = math.max(tr_tmp, math.abs(L(i) - C(i - 1)))

    }
  }

  def tr(idx: Int): Double = {
    var i = 0

    while (i < C.length) {
      computeSpot(i)
      i += 1
    }

    _tr(idx)
  }

}

