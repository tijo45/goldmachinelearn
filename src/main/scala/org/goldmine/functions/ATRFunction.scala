package org.goldmine.functions

import org.goldmine.indicator.Factor

class ATRFunction(val H: Array[Double], val L: Array[Double], val C: Array[Double], val periodAtr: Factor) {

  val tr = new TRFunction(H, L, C)

  def atr(index:Int):Double =
    if (index<=0) 0.0
    else {
      val period = periodAtr.value
      (atr(index-1) * (period - 1.0) + tr.tr(index))/period
    }
}
