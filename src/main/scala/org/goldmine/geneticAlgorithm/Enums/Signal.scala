package org.goldmine.geneticAlgorithm.Enums

object Signal extends Enumeration {
  type Signal = Value
  val >, < = Value
}