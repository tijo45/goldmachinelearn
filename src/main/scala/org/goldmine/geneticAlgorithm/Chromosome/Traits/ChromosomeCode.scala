package org.goldmine.geneticAlgorithm.Chromosome.Traits

import org.goldmine.geneticAlgorithm.Enums.Signal.Signal

trait ChromosomeCode {
  var emas_periods: Int
  var emal_bool: Boolean
  var emal_periods: Int
  var tr1_bool: Boolean
  var tr1_periods: Int
  var tr1_percent: Int
  var tr1_signal: Signal
  var tr2_bool: Boolean
  var tr2_signal: Signal
  var tr2_times: Int
  var tr2_percent: Int
  var tr2_periods: Int
  var p1: Int
  var sl1: Int
  var tp1: Int
  var fast_bool: Boolean
  var fast_tp: Int
  var fast_sl: Int
  var fast_percent: Int
  var fast_periods: Int
  var slow_bool: Boolean
  var slow_tp: Int
  var slow_sl: Int
  var slow_percent: Int
  var slow_periods: Int
  var better_bool: Boolean
  var better_tp: Int
  var better_sl: Int
  var worse_bool: Boolean
  var worse_tp: Int
  var worse_sl: Int
}
