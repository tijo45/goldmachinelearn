package org.goldmine.geneticAlgorithm.Chromosome

import org.goldmine.geneticAlgorithm.Chromosome.Traits.ChromosomeCode

class Chromosome(chromosomeCode: ChromosomeCode) {
  private var _code: ChromosomeCode = chromosomeCode

  def code = _code
  def code_ (newCode: ChromosomeCode): Unit = {
    _code = newCode
  }

  def evaluateChromosome(): Boolean = {
    if(_code.emas_periods < 3 || _code.emas_periods > 50) {
      return false
    }
    else if (_code.emal_periods < 10 || _code.emal_periods > 200) {
      return false
    }
    else if (_code.tr1_periods < 4 || _code.tr1_periods > 25) {
      return false
    }
    else if (_code.tr1_percent < 1 || _code.tr1_percent > 100) {
      return false
    }
    else if (_code.tr2_times < 3 || _code.tr2_times > 24) {
      return false
    }
    else if (_code.tr2_percent < 50 || _code.tr2_percent > 99) {
      return false
    }
    else if (_code.tr2_periods < 4 || _code.tr2_periods > 25) {
      return false
    }
    else if (_code.p1 < -20 || _code.p1 > 20) {
      return false
    }
    else if (_code.sl1 < 1 || _code.sl1 > 100) {
      return false
    }
    else if (_code.tp1 < 1 || _code.tp1 > 100) {
      return false
    }
    else if (_code.fast_percent < 1 || _code.fast_percent > 100) {
      return false
    }
    else if (_code.fast_tp < 1 || _code.fast_tp > 100) {
      return false
    }
    else if (_code.fast_sl < 1 || _code.fast_sl > 100){
      return false
    }
    else if (_code.fast_periods < 1 || _code.fast_periods > 40) {
      return false
    }
    else if (_code.slow_percent < 1 || _code.slow_percent > 100) {
      return false
    }
    else if (_code.slow_tp < 1 || _code.slow_tp > 100) {
      return false
    }
    else if (_code.slow_sl < 1 || _code.slow_sl > 100) {
      return false
    }
    else if (_code.slow_periods < 1 || _code.slow_sl > 100) {
      return false
    }
    else if (_code.slow_percent < 1 || _code.slow_percent > 40) {
      return false
    }
    else if (_code.better_sl < 1 || _code.better_sl > 100) {
      return false
    }
    else if (_code.better_tp < 1 || _code.better_tp > 100) {
      return false
    }
    else if (_code.worse_sl < 1 || _code.worse_sl > 100) {
      return false
    }
    else if (_code.worse_tp < 1 || _code.worse_tp > 100) {
      return false
    }
    else {
      return true
    }
  }
}
