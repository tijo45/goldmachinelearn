//package org.akkatrading.backtest.strategy
//
//import akka.actor.{ActorLogging, Actor, FSM}
//import org.akkatrading.backtest.model.{CandleWithIndicators}
//import org.akkatrading.backtest.strategy.StrategyFSM._
//import org.goldmine.functions._
//import org.goldmine.indicator.Factor
//
//import scala.collection.mutable.ArrayBuffer
//
//object BacktestStrategyFSM {
//
//  sealed trait State
//
//  case object Flat extends State
//
//  sealed trait Data
//
//  case object Empty extends Data
//
//}
//
//class BacktestStrategyFSM extends Actor with FSM[State, Data] with ActorLogging {
//
//  startWith(Flat, Empty)
//
//  val O = new ArrayBuffer[Double]
//  val H = new ArrayBuffer[Double]
//  val L = new ArrayBuffer[Double]
//  val C = new ArrayBuffer[Double]
//
//  when(Flat) {
//    case Event(tick: CandleWithIndicators, data) =>
//      O += tick.open
//      H += tick.high
//      L += tick.low
//      C += tick.close
//
//      val period5 = new Factor("Period 5 EMA", 5)
//      val period60 = new Factor("Period 60 EMA", 60)
//      val period200 = new Factor("Period 200 EMA", 200)
//
//      val openSlice = O.toArray //quotes.toArray.slice(quotes.toArray.length - period*2, quotes.toArray.length)
//      val highSlice = H.toArray
//      val lowSlice = L.toArray
//      val closeSlice = C.toArray
//
//      val ema5 = new EMAFunction(closeSlice, period5)
//      val ema60 = new EMAFunction(closeSlice, period60)
//      val ema200 = new EMAFunction(closeSlice, period200)
//
//      val periodDi = new Factor("Period Di", 14)
//      val periodAdx = new Factor("Period Adx", 14)
//      //val adxMain = new ADXFunction(highSlice, lowSlice, closeSlice, periodDi, periodAdx)
//      //val dxMain = new DXFunction(highSlice, lowSlice, closeSlice, periodDi)
//      val diMain = new DIFunction(highSlice, lowSlice, closeSlice, periodDi)
//      val dmFunction = new DMFunction(highSlice, lowSlice, closeSlice)
//      val trFunction = new TRFunction(highSlice, lowSlice, closeSlice)
//      val dxFunction = new DXFunction(highSlice, lowSlice, closeSlice, periodDi)
//      val adxFunction = new ADXFunction(highSlice, lowSlice, closeSlice, periodDi, periodAdx)
//
//      if (closeSlice.length > 1) {
//        //; adxPlusDi=%f; adxPlusDiPrev=%f; adxMinusDi=%f; adxMinusDiPrev=%f; adxMain=%f; adxMainPrev=%f; atr=%f; atrPrev=%f; atr1Hour=%f; atr1HourPrev=%f
//        log.info("I've got a candle! %s; ema5=%f; ema60=%f; ema200=%f; tr=%f; dm1Plus=%f; dm1Minus=%f; tr14=%f; di14Plus=%f; di14Minus=%f; dx=%f; adx=%f".format(
//          tick, ema5.compute(), ema60.compute(), ema200.compute(), trFunction.tr(closeSlice.length-1), dmFunction.dmPlus(closeSlice.length-1),
//          dmFunction.dmMinus(closeSlice.length-1), diMain.tr(closeSlice.length-1), diMain.diPlus(closeSlice.length-1), diMain.diMinus(closeSlice.length-1),
//          dxFunction.dx(closeSlice.length-1), adxFunction.adx(closeSlice.length-1)))
//      }
//
//      stay()
//  }
//
//  whenUnhandled {
//    case Event(e, s) =>
//      log.warning("Received unhandled request {} in state {}/{}", e, stateName, s)
//      stay()
//  }
//
//  initialize()
//}
