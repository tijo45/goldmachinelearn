package org.akkatrading.backtest

import java.time.{ZoneId, ZonedDateTime, LocalDateTime}
import java.time.format.DateTimeFormatter

import akka.actor.Actor
import org.akkatrading.backtest.PriceDataCsvReader.{GetLastReadTicks, PriceTicks, ReadTicksFromCsv}
import org.akkatrading.backtest.model.CandleWithIndicators

import scala.io.Source
import org.akkatrading.live.PriceListener.Tick

object PriceDataCsvReader {

  case class ReadTicksFromCsv(fileName: String)
  case object GetLastReadTicks

  case class PriceTicks(priceTicks: List[Tick])
}

class PriceDataCsvReader extends Actor {

  val dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu.MM.dd HH:mm:ss.SSS") //"dd.MM.uuuu HH:mm:ss"
  
  private var ticks: List[Tick] = Nil

  def receive = {
    case ReadTicksFromCsv(fileName: String) =>
      ticks = Source.fromFile(fileName).getLines().map {
        line =>
          val tokens = line.split(";")
          Tick(tokens(1),
            LocalDateTime.parse(tokens(0), dateTimeFormatter).atZone(ZoneId.of("UTC")),
            tokens(2).toDouble,
            tokens(3).toDouble
          )
//          CandleWithIndicators(LocalDateTime.parse(tokens(0), dateTimeFormatter).atZone(ZoneId.systemDefault()),
//              tokens(1).toDouble, tokens(1).toDouble, tokens(1).toDouble,
//              tokens(4).toDouble, tokens(4).toDouble, tokens(4).toDouble,
//              tokens(7).toDouble, tokens(7).toDouble, tokens(7).toDouble,
//              tokens(10).toDouble, tokens(10).toDouble, tokens(10).toDouble,
//              1, true,
//              tokens(13).toDouble, tokens(14).toDouble, tokens(15).toDouble,   //EMA 5, EMA 60, EMA 200
//              tokens(16).toDouble, tokens(17).toDouble, //ADX +DI, ADX +DI Prev
//              tokens(18).toDouble, tokens(19).toDouble,  //ADX -DI, ADX -DI Prev
//              tokens(20).toDouble, tokens(21).toDouble,  //ADX Main, ADX Main Prev
//              tokens(22).toDouble, tokens(23).toDouble,  //ATR 1m, ATR 1m Prev
//              tokens(24).toDouble, tokens(25).toDouble  //ATR 1h, ATR 1h Prev
//            )
      }.toList
      println("who called me??")
      Thread.currentThread().getStackTrace().foreach { println(_) }
      sender() ! PriceTicks(ticks)
    
    case GetLastReadTicks =>
      println("who called me?")
      sender() ! PriceTicks(ticks)
  }
}
