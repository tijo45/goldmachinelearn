package org.akkatrading.backtest

import java.text.DecimalFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.ZonedDateTime
import java.time.ZoneId

import scala.concurrent.duration._
import scala.io.Source

import org.akkatrading.backtest.PriceDataCsvReader.PriceTicks
import org.akkatrading.backtest.PriceDataCsvReader.ReadTicksFromCsv
import org.akkatrading.backtest.model.CandleWithIndicators
import org.akkatrading.backtest.model.LogMessage
import org.akkatrading.live.CandleFetcher.Candle
import org.akkatrading.live.CandleFetcher.FetchCandles
import org.akkatrading.live.CurrentPrice
import org.akkatrading.live.OrderCanceler.CancelOrderRequest
import org.akkatrading.live.OrderCreator.CreateOrderRequest
import org.akkatrading.live.PriceListener.Tick
import org.akkatrading.live.StrategyFSM
import org.akkatrading.live.TradeModifier.ModifyTradeRequest
import org.akkatrading.live.strategy.BlueiceStrategy
import org.akkatrading.live.util.Instrument
import org.akkatrading.live.util.TimeFrameEnum.TimeFrameVal
import org.akkatrading.live.util.Side.SideVal

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.testkit.TestActorRef
import akka.testkit.TestFSMRef
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal

class LoggingSpy extends Actor with ActorLogging {
  override def receive: Receive = {
    case cp: CurrentPrice =>
    case any: AnyRef => {
//      println(s"Event: ${any.toString}}")
    }
  }
}

case class AccountTrade(instrument: InstrumentVal, side: SideVal,
        open: Double, openTime: ZonedDateTime,
        close: Double, closeTime: ZonedDateTime,
        units: Int) {
  def isProfitable = (side == "buy" && open < close) || (side == "sell" && close < open)
  def profit = Math.abs(close - open) * units * (if (isProfitable) 1 else -1)
}

case object HistoryRequest
case class HistoryResponse(trades: List[AccountTrade]) {
  def getProfitable = trades.filter {_.isProfitable}
  def getNegative = trades.filter {!_.isProfitable}
  
  def profit = trades.foldLeft(0d)((z, trade) => z + trade.profit)
}

class Market(orderManager: ActorRef) extends Actor with ActorLogging {
  import org.akkatrading.live.AccountFetcher._
  import org.akkatrading.live.OrderFetcher._
  
  var id: Long = 1
  var trades: Map[Long, Trade] = Map()
  
  var lastTickOption: Option[Tick] = None
  var tradeHistory: List[AccountTrade] = Nil
  
  def receive = {
    case AccountRequest =>
      val response = AccountResponse(0, "a", 100, 10, 10, 10, 10, 10, 10, 10, "")
      orderManager ! response
    case FetchOrderRequest(maxId, count, instrument, ids) =>
      orderManager ! new FetchTradeResponse(trades.values.toList)
    
    case tick: Tick =>
      trades = trades.filter {
        case (id, Trade(_, instrument, units, side, openTime, price, takeProfit, _, stopLoss, _)) =>
          if (side == "buy") {
            if ((stopLoss != 0.0 && tick.bid < stopLoss) || (takeProfit != 0.0 && takeProfit < tick.bid)) {
              tradeHistory = AccountTrade(instrument, side, price, openTime, tick.bid, tick.time, units) :: tradeHistory 
              false
            } else {
              true
            }
          } else if ((stopLoss != 0.0 && stopLoss < tick.ask) || (takeProfit != 0.0 && tick.ask < takeProfit)) {
            tradeHistory = AccountTrade(instrument, side, price, openTime, tick.ask, tick.time, units) :: tradeHistory
            false
          } else {
            true
          }
      }
      lastTickOption = Some(tick)
    
    case CreateOrderRequest(instrument, units, side, _, _, _, _, _, _, _) =>
      id += 1
      val tick = lastTickOption.get
      val (price, takeProfit, stopLoss) = if (side == "buy") {
//        (tick.ask, Double.MaxValue, 0d)
        (tick.ask, 0d, 0d)
      } else {
//        (tick.bid, 0d, Double.MaxValue)
        (tick.bid, 0d, 0d)
      }
      val newTrade = Trade(id, instrument, units, side, tick.time, price, takeProfit, 0, stopLoss, 0)
      println(newTrade)
      trades += (id -> newTrade)
    
    case ModifyTradeRequest(id, instrument, Some(stopLoss), _, _) =>
      val trade = trades(id)
      val updatedTrade = trade.copy(stopLoss = stopLoss)
      trades += (id -> updatedTrade)
    
    case CancelOrderRequest(id, units, instrument) =>
      val trade = trades(id)
      val updatedTrade = trade.copy(units = trade.units - units)
      if (updatedTrade.units == 0) {
        trades -= id
      } else {
        trades += (id -> updatedTrade)
      }
      val tick = lastTickOption.get
      val endPrice = if (trade.side == "buy") tick.bid else tick.ask
      tradeHistory = AccountTrade(instrument, trade.side, trade.price, trade.time, endPrice, tick.time, units) :: tradeHistory
    
    case HistoryRequest =>
      sender ! HistoryResponse(tradeHistory)
  }
}

case class SetAccountRef(accountInfo: ActorRef)

class OrderRecorder(csvReader: ActorRef) extends Actor with ActorLogging {
  
  import org.akkatrading.live.StrategyFSM.OrderResponseSuccess
  
  var accountInfoOption: Option[ActorRef] = None
  
  def receive = {
    case SetAccountRef(accountInfo) =>
      accountInfoOption = Some(accountInfo)
    case request @ CreateOrderRequest(instrument, _, _, _, _, _, _, _, _, _) =>
      accountInfoOption.get ! request
      sender() ! new OrderResponseSuccess(instrument)
    case request @ CancelOrderRequest(_, _, instrument) =>
      accountInfoOption.get ! request
      sender() ! new OrderResponseSuccess(instrument)
    case request @ ModifyTradeRequest(_, instrument, _, _, _) =>
      accountInfoOption.get ! request
      sender() ! new OrderResponseSuccess(instrument) 
  }
}

class BacktestCandleFetcher(timeframeCandles: Map[TimeFrameVal, Iterable[Candle]]) extends Actor with ActorLogging {
  import org.akkatrading.live.CandleFetcher._
  
  def receive = {
    case FetchCandles(instrument, count, granularity, candleFormat, Some(till)) =>
      timeframeCandles.get(granularity) match {
        case Some(candles) =>
          val result = candles.takeWhile {c =>
            c.time.isBefore(till) || c.time.isEqual(till)
          }.takeRight(count)
          if (result.size < count) {
            log.error(s"Timeframe with ${granularity} doesn't have enough history")
          } else if (count == 2) {
            sender ! PriceUpdate(instrument, granularity, result.tail.head, result.head)
          } else {
            sender ! BulkPriceUpdate(instrument, granularity, result.toList)
          }
        case None =>
          log.error("No timeframe with granularity: " + granularity)
      }
    case other => log.error("Received something unexpected: {}", other)
  }
}

class Backtest extends Actor with ActorLogging {

  import context._
  import org.akkatrading.live.AccountFetcher._
  import org.akkatrading.live.OrderFetcher._
  import org.akkatrading.live.util.InstrumentEnum._
  import org.akkatrading.live.util.TimeFrameEnum._

  val formatter = new DecimalFormat("#.######")
  val csvReader = actorOf(Props[PriceDataCsvReader], "csvReader")
  val instrumentVal = EUR_CAD
  val timeframes = List(M1, M5, H1)

  override def preStart() {
//    csvReader ! ReadTicksFromCsv("src/main/resources/GBPUSD_1_1454643595_candles.csv")
//    csvReader ! ReadTicksFromCsv("src/main/resources/candles.csv")
    csvReader ! ReadTicksFromCsv("src/main/resources/ticks_EUR_CAD_1476804592597.csv")
  }

  context.setReceiveTimeout(360 second)

  def receive = {
    case PriceTicks(priceTicks) =>
      val loggingSpy = TestActorRef(Props[LoggingSpy])
      val orderRecorder = TestActorRef(Props(classOf[OrderRecorder], csvReader), "orderRecorderMock")
      val candleFetcher = TestActorRef(Props(classOf[BacktestCandleFetcher], Map()/*readHistoryHandles()*/), "candleFetcher")
      val orderManager = TestFSMRef(new StrategyFSM(
        loggingSpy,
        instrumentVal,
        orderRecorder, orderRecorder, orderRecorder,
        candleFetcher,
        new BlueiceStrategy(loggingSpy),
        true
      ))
      
      val ticksIteration = priceTicks.iterator
      val market = TestActorRef(Props(classOf[Market], orderManager), "accountInfoFetcher")
      orderRecorder ! SetAccountRef(market)
      market ! AccountRequest
      val tick = ticksIteration.next()
      1 to 7 foreach {_ =>
        orderManager ! ticksIteration.next()
      }
      
      market ! FetchOrderRequest(None, None, None, None)
      for (tick <- ticksIteration) {
        println(tick)
        market ! tick
        market ! FetchOrderRequest(None, None, None, None)
        orderManager ! tick
      }
      
      market ! HistoryRequest
    case LogMessage(d, intervalPnl, tradePnl, logMsg) =>
      println(s"${DateTimeFormatter.ofPattern("dd.MM.uuuu\tHH:mm:ss").format(d)}\t${formatter.format(intervalPnl)}\t${formatter.format(tradePnl)}\t$logMsg")
    case r @ HistoryResponse(trades) =>
      println("Profit: " + r.profit)
      println(trades.map(t =>
        s"Side: ${t.side}; \tOpen: ${t.open}; \tClose: ${t.close}; \tProfit: ${t.profit}"
      ).mkString("Trades:\n", "\n", ""))
      stop(self)
    case ReceiveTimeout =>
      stop(self)
  }

  override def postRestart(reason: Throwable): Unit = {
    reason.printStackTrace()
    super.postRestart(reason)
  }
  
  val dateTimeFormat = DateTimeFormatter.ofPattern(CandleHistoryFetcherApp.datePattern)
  def readHistoryHandles(): Map[TimeFrameVal, Iterable[Candle]] = timeframes.map({timeframe =>
    val candles = Source.fromFile(s"src/main/resources/${instrumentVal}_${timeframe}_candles.csv").getLines().map {line =>
      val cells = line.split(";")
      Candle(
        LocalDateTime.parse(cells(0), dateTimeFormat).atZone(ZoneId.of("UTC")),
        cells(1).toDouble, cells(2).toDouble,
        cells(3).toDouble, cells(4).toDouble,
        cells(5).toDouble, cells(6).toDouble,
        cells(7).toDouble, cells(8).toDouble,
        cells(9).toInt,
        true)
    }.toVector
    timeframe -> candles
  }).toMap
}
