package org.akkatrading.backtest

import scala.concurrent.duration._
import scala.io.Source

import java.time.ZonedDateTime
import java.io.File

import akka.actor.{ ActorSystem, Inbox, Props }
import akka.pattern.ask
import akka.util.Timeout
import akka.io.IO

import spray.can.Http
import spray.can.Http.HostConnectorInfo

import org.akkatrading.live.CandleFetcher
import org.akkatrading.live.CandleFetcher.BulkPriceUpdate
import org.akkatrading.live.CandleFetcher.FetchCandles
import org.akkatrading.live.util.InstrumentEnum
import org.akkatrading.live.util.InstrumentEnum._
import org.akkatrading.live.util.TimeFrameEnum
import org.akkatrading.live.util.TimeFrameEnum._
import java.text.SimpleDateFormat
import java.io.PrintWriter
import java.time.ZoneId
import java.time.ZoneOffset
import java.util.Date
import java.util.TimeZone

object CandleHistoryFetcherApp {
  implicit val timeout = Timeout(5 seconds)
  val datePattern = "yyyy.MM.dd HH:mm:ss"
  private val dateTimeFormat = new SimpleDateFormat(datePattern)
  dateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
  val zoneId = ZoneId.of("UTC")
  
  def main(args: Array[String]): Unit = {
    implicit val actorSystem = ActorSystem("actor-system")
    import actorSystem.dispatcher
    
    val (instrument, count, granularity, year, month, day) = if (args.size == 6) {
      (InstrumentEnum.fromStr(args(0)), args(1).toInt, TimeFrameEnum.fromStr(args(2)), args(3).toInt, args(4).toInt, args(5).toInt)
    } else {
      (EUR_CAD, 5000, M5, 2016, 10, 10)
    }
    
    val inbox = Inbox.create(actorSystem)
    
    val connectorSetup = Http.HostConnectorSetup("api-fxpractice.oanda.com", port = 443, sslEncryption = true)
    val connectorSetupFuture = IO(Http) ? connectorSetup
    connectorSetupFuture.map {
      case HostConnectorInfo(hostConnector, _) =>
        val candleFetcher = actorSystem.actorOf(Props(classOf[CandleFetcher], hostConnector))
        val until = ZonedDateTime.of(year, month, day, 0, 0, 0, 0, zoneId)
        val message = FetchCandles(instrument, count, granularity, "bidask", None)//Some(until))
        inbox.send(candleFetcher, message)
    }
    
    val BulkPriceUpdate(_, _, candles) = inbox.receive(60 seconds)
    val file = new File(s"src/main/resources/${instrument.desc}_${granularity.desc}_candles.csv")
    val writer = new PrintWriter(file)
    try {
      candles.foreach {candle =>
        val line = List(
          dateTimeFormat.format(candle.time.withZoneSameLocal(zoneId).toEpochSecond() * 1000),
          candle.openBid, candle.openAsk,
          candle.highBid, candle.highAsk,
          candle.lowBid, candle.lowAsk,
          candle.closeBid, candle.closeAsk,
          candle.volume
        )
        writer.println(line.mkString(";"))
      }
    } finally {
      writer.close()
      actorSystem.terminate()
    }
  }
}
