package org.akkatrading.live.util

import java.time.temporal.ChronoUnit
import java.time.ZonedDateTime

import org.akkatrading.live.PriceListener.Tick
import org.akkatrading.live.util.Quotes.FullCandle
import org.akkatrading.live.CandleFetcher.Candle
import org.goldmine.math.PatchedStatsFunctions._

import scala.collection.immutable.::

class TimeFrameImmutable private (size:Int, granularity:Int, quotes:List[FullCandle]) {

  def handleBulkPriceUpdate(candles:List[FullCandle]) = {
    def merge(incoming:List[FullCandle], existing:List[FullCandle]):List[FullCandle] =
      incoming.foldLeft(existing) {
        case (acc, item) => acc.takeWhile(_.time.compareTo(item.time)>0) ++ (item::acc.dropWhile(_.time.compareTo(item.time)>=0))
      }

    TimeFrameImmutable(
      size,
      granularity,
      merge(
        candles
          .groupBy(_.time)
          .map {case (k,v) => v.head}
          .toList
          .sortWith { case (c1, c2) => c1.time.compareTo(c2.time) > 0 },
        quotes
      )
    )
  }

  def isConsistent =
    quotes.isEmpty ||
      quotes
        .tail
        .zip(quotes)
        .forall {case (prev, cur) => cur.time.minusMinutes(granularity).compareTo(prev.time)==0}

  def candles = quotes

  def lastCandle: FullCandle = quotes.headOption.getOrElse(FullCandle(Candle(ZonedDateTime.now(), 0, 0, 0, 0, 0, 0, 0, 0, 1, false)))

  def lastButOneCandle: Option[FullCandle] = quotes match {
    case (first::rest) => rest.headOption
    case _ => None
  }



  def handleTick(tick: Tick) = quotes match {
    case Nil => TimeFrameImmutable(size, granularity, FullCandle.newCandleFromTick(tick, granularity)::Nil)
    case (fresh::rest) =>
      if (fresh.time == tick.time.truncatedTo(ChronoUnit.MINUTES) || (tick.time.compareTo(fresh.time.plusMinutes(granularity)) < 0)) {
        TimeFrameImmutable(size, granularity, FullCandle.updateCurrentWithTick(fresh, tick, false)::rest)
      } 
      else {  
        if ((tick.time.getMinute >= (fresh.time.getMinute + granularity)) 
            || (tick.time.getMinute == 0 && fresh.time.getMinute == 59) 
            || (tick.time.getHour > fresh.time.getHour)
            || (tick.time.getDayOfMonth > fresh.time.getDayOfMonth)
            || (tick.time.getMonthValue > fresh.time.getMonthValue)
            || (tick.time.getYear > fresh.time.getYear)
            ) {
          if(granularity > 10) System.out.println("Updating the candle for " + granularity + " at time : " + tick.time.toString())
          val uq = FullCandle.newCandleFromTick(tick, granularity)::FullCandle.update(fresh, completed = true)::rest
          val t = TimeFrameImmutable(size, granularity,uq)
          t
        } 
        else {
          System.out.println("Something went wrong - returning current TFI for " + granularity)
          TimeFrameImmutable(size, granularity,fresh::rest)
        }
      }
  }

  //Makes simple checks of candles integrity up to 2 candles deep.
  def handlePriceUpdate(previous:FullCandle, current:FullCandle) =
    quotes match {
      case Nil => TimeFrameImmutable(size, granularity, current::previous::quotes)
      case (first::rest) =>
        current.time.compareTo(first.time) match {
          case v if v>0 =>
            if (previous.time == first.time)
              TimeFrameImmutable(size, granularity, current::first::rest)
            else
              this
          case v if v==0 =>
            rest match {
              case Nil => TimeFrameImmutable(size, granularity, previous::quotes)
              case (second::rrest) =>
                if (previous.time == second.time) {
                  TimeFrameImmutable(size, granularity, first::previous::rrest)
                } else {
                  this
                }
            }
          case _ => this
        }
    }

  def enoughCandles() = quotes.size >= size
 
  override def toString = s"[size=$size, granularity=$granularity, quotes=$quotes]"
}

object TimeFrameImmutable {
  def apply(size:Int, granularity:Int, quotes:List[FullCandle] = Nil) = new TimeFrameImmutable(size, granularity, quotes.take(size))

}