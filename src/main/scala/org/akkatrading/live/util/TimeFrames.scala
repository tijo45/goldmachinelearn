package org.akkatrading.live.util

import org.akkatrading.live.CandleFetcher.{BulkPriceUpdate, PriceUpdate}
import org.akkatrading.live.PriceListener.Tick
import org.akkatrading.live.util.Quotes.FullCandle
import org.akkatrading.live.util.TimeFrameEnum.TimeFrameVal
import org.slf4j.LoggerFactory

class TimeFrames(values:Map[TimeFrameVal, TimeFrameImmutable]) {

  val log = LoggerFactory.getLogger(this.getClass)

  def handleTick(tick: Tick, granularity:TimeFrameVal): TimeFrames =
    values
      .get(granularity)
      .fold {
        log.error(s"Timeframe of $granularity not found")
        this
      } (
        tf => new TimeFrames(values + (granularity -> tf.handleTick(tick)))
      )

  def handlePriceUpdate(priceUpdate: PriceUpdate): TimeFrames =
    values
      .get(priceUpdate.granularity)
      .fold {
        log.error(s"Timeframe of ${priceUpdate.granularity} not found")
        this
      } (
        tf => new TimeFrames(values + (priceUpdate.granularity -> tf.handlePriceUpdate(FullCandle(priceUpdate.previous), FullCandle(priceUpdate.current))))
      )

  def handleBulkPriceUpdate(bulkPriceUpdate:BulkPriceUpdate): TimeFrames =
    values
      .get(bulkPriceUpdate.granularity)
      .fold{
        log.error(s"Timeframe of ${bulkPriceUpdate.granularity} not found")
        this
      } (
        tf => new TimeFrames(values + (bulkPriceUpdate.granularity -> tf.handleBulkPriceUpdate(bulkPriceUpdate.candles.map(FullCandle.apply))))
      )

  def enoughCandles(granularity:TimeFrameVal): Boolean =
    values
      .get(granularity)
      .fold {
        log.error(s"Timeframe of $granularity not found")
        true
      } (
        _.enoughCandles()
      )

  def apply(granularity:TimeFrameVal) = values.get(granularity)
  
  override def toString = values.mkString("\n")
}

