package org.akkatrading.live

trait OuterControl

trait In
trait Out

case class TradeState(id: Long, instrument: String, side:String, opening: Double, `type`:String = "tradeChanged") extends Out

case class CurrentPrice(instrument:String,
                        current:Double,
                        indicators:Map[String, Double],
                        `type`:String = "curPrice") extends Out

case class TradeClosed(id: Long, instrument: String, `type`:String = "tradeClosed") extends Out

case class StateChanged(to:String, `type`:String = "stateChanged") extends Out

case class CloseTrade(id: Long) extends In



