package org.akkatrading.live

import java.time.ZonedDateTime
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import org.akkatrading.live.util.DateUtils._
import spray.client.pipelining._
import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.json.DefaultJsonProtocol
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import org.akkatrading.live.OrderFetcher.FetchTradeResponse
import org.akkatrading.live.OrderFetcher.FetchTradeResponse
import org.akkatrading.live.OrderFetcher.FetchOrderRequest
import org.akkatrading.live.AccountFetcher.AccountRequest
import org.akkatrading.live.AccountFetcher.AccountResponse
import org.akkatrading.live.AccountFetcher.AccountResponse
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal

object AccountFetcher {

  
  //http://stackoverflow.com/questions/20673091/what-message-does-pipeto-send-on-a-timeout-or-other-failure
  
  def props(connector: ActorRef, instr: Set[InstrumentVal]): Props = Props(new AccountFetcher(connector, instr))

  //count=50 by default
  //ids - An URL encoded comma (%2C) separated list of orders to retrieve. Maximum number of ids: 50.
  case class AccountRequest()


  case class AccountResponse(accountId: Double, accountName: String, balance: Double, unrealizedPl: Double,
                         realizedPl: Double, marginUsed: Double, marginAvail: Double, openTrades: Double, 
                         openOrders: Double, marginRate: Double, accountCurrency: String)



  object AccountJsonProtocol extends DefaultJsonProtocol {
    implicit val accountFormat = jsonFormat11(AccountResponse)
  }

}

class AccountFetcher(connector: ActorRef, instr: Set[InstrumentVal]) extends Actor with ActorLogging with AuthInfo {

  import context.dispatcher

  implicit val timeout = Timeout(10 seconds)
  
  import org.akkatrading.live.AccountFetcher.AccountJsonProtocol._

// context.system.scheduler.schedule(0 milliseconds, 5000 milliseconds, self, FetchOrderRequest(None, None, None, None))
 
  val logResponse: HttpResponse => HttpResponse = {
    r => log.info("AccountResponse: {}", r)
    r
  }
  
  val pipeline = addCredentials(OAuth2BearerToken(authToken)) ~> sendReceive(connector) ~> unmarshal[AccountResponse]
  
  

  def receive = {
    case AccountRequest =>
//      val orderManager = context.system.actorSelection("akka://oanda-client/user/orderManager" + instr).resolveOne().onComplete {
        pipeline(Get(s"/v1/accounts/$accountId")) onComplete {
          case Success(accountResponse: AccountResponse) =>
            instr foreach { i =>
              context.system.actorSelection("akka://oanda-client/user/orderManager" + i.desc.toString).resolveOne().onComplete {
                omtry => omtry.map { om => om ! accountResponse } 
              }
            }
            
    
          case Success(somethingUnexpected) =>
            log.warning("The Oanda API call was successful but returned something unexpected: '{}'.", somethingUnexpected)
    
          case Failure(error) =>
            log.error(error, "Couldn't fetch accounts")
          }
      
    }
}

