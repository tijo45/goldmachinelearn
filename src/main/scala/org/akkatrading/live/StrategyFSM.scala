package org.akkatrading.live

import akka.actor.{ActorRef, FSM, Props}
import org.akkatrading.live.CandleFetcher.{BulkPriceUpdate, FetchCandles, PriceUpdate}
import org.akkatrading.live.OrderCanceler.{CancelOrderConfirmation,  CancelOrderRequest}
import org.akkatrading.live.OrderCreator.{CreateOrderConfirmation, CreateOrderRequest}
import org.akkatrading.live.OrderFetcher.{FetchOrderRequest, FetchTradeResponse, Trade}
import org.akkatrading.live.PriceListener.Tick
import org.akkatrading.live.StrategyFSM.{State, _}
import org.akkatrading.live.strategy.Strategy
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
import org.akkatrading.live.util._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import org.akkatrading.live.AccountFetcher.AccountResponse
import org.akkatrading.live.AccountFetcher.AccountResponse
import org.akkatrading.live.TradeModifier.ModifyTradeRequest
import org.akkatrading.live.TradeModifier.ModifyTradeConfirmation
import java.time.ZonedDateTime
import org.akkatrading.live.OrderCreator.CreateOrderRequest
import org.akkatrading.live.TradeModifier.ModifyTradeRequest
import org.akkatrading.live.OrderCanceler.CancelOrderRequest
import scala.collection.mutable.ArrayBuffer

object StrategyFSM {

  def props(
    webLog:ActorRef,
    instrument:InstrumentVal,
    tradeModifier: ActorRef,
    orderCreator: ActorRef,
    orderCanceler: ActorRef,
    candleFetcher: ActorRef,
    strategy: Strategy
  ) = Props(new StrategyFSM(webLog, instrument, tradeModifier, orderCreator, orderCanceler, candleFetcher, strategy))

  sealed trait State

  case object BeforeStart extends State

  case object DownloadingMissingCandles extends State

  case object SendingOrder extends State

  case object Trading extends State

  sealed trait Data
  
  type PrecisionType = Double
  type OptionMap = Map[Symbol, Any]
  type Pattern = Array[PrecisionType]
  
  class OrderRequest(instrument : String, id : Long) {
    def i = instrument
    def orderId = id
  }
  class OrderResponseSuccess(instrument : String) {
    def i = instrument
  }
  class OrderResponseError(instrument : String, error : String) {
    def i = instrument
    def e = error
  }

  final case class StrategyState(orders: List[Trade],
        allAveragePrices:ArrayBuffer[PrecisionType], patterns: ArrayBuffer[Pattern], performances: ArrayBuffer[PrecisionType], 
        accountInfo: AccountResponse, initialBalance : Double , tickCount: Int = 0 ) extends Data

  final case class OrderToSend(orderRequests: List[OrderRequest], orders: List[Trade], 
      allAveragePrices:ArrayBuffer[PrecisionType], patterns: ArrayBuffer[Pattern], performances: ArrayBuffer[PrecisionType], 
      accountInfo: AccountResponse, initialBalance : Double, errorCount: Int = 0, lastRequest: OrderRequest) extends Data
  
  
}

class StrategyFSM(
    webLog:ActorRef,
    instrument:InstrumentVal,
    tradeModifier: ActorRef,
    orderCreator: ActorRef,
    orderCanceler: ActorRef,
    candleFetcher: ActorRef,
    strategy: Strategy,
    startFetching: Boolean = true
) extends FSM[State, Data] with AuthInfo {

  import org.akkatrading.live.StrategyFSM.OrderResponseError
  import org.akkatrading.live.StrategyFSM.OrderResponseSuccess
  import org.akkatrading.live.OrderCreator.CreateOrderRequest
  
  implicit val timeout: akka.util.Timeout = akka.util.Timeout(15 seconds)

  val preloadCandlesCount = 50 // Maximum candles sufficient for indicators
  
  val tickWriter = context.actorOf(TickWriter.props(instrument.desc), "tickWriter")
  
  implicit def ordered: Ordering[ZonedDateTime] = new Ordering[ZonedDateTime] {
    def compare(x: ZonedDateTime, y: ZonedDateTime): Int = y compareTo x
  }

  startWith(
    BeforeStart,
    StrategyState(
      List.empty,
      new ArrayBuffer[PrecisionType](),
      new ArrayBuffer[Pattern](),
      new ArrayBuffer[PrecisionType](),
      new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 0.0
    )
  )

  def reportTradesUpdate(trades: List[Trade], target:ActorRef = webLog) = {
//    reportStateUpdate(stateName.toString, target)
//    trades.foreach(item => target ! TradeState(item.id, item.instrument, item.side, item.price))
  }

  when(BeforeStart) {
    case Event(account : AccountResponse, data: StrategyState) =>
      if(data.initialBalance == 0.00) {
        stay() using data.copy(accountInfo = account, initialBalance = account.balance)
      }
      else {
          stay() using data.copy(accountInfo = account)
        }
    case Event(tradeResponse : FetchTradeResponse, data: StrategyState) =>
      reportTradesUpdate(tradeResponse.trades)
     // tradeResponse.trades.sortBy { x => x.id }.reverse foreach { trade => println(trade.id);orderCanceler ! CancelOrderRequest(trade.id, trade.units, trade.instrument) }
     // println("Closed all trades! : " + tradeResponse.trades.length)
        goto(Trading) using data.copy(orders = tradeResponse.trades)
      
    case Event(tick: Tick, data: StrategyState) =>
      if(tick.instrument == instrument.toString()) {
//        tickWriter ! tick
        data.allAveragePrices :+ (tick.bid + tick.ask) / 2
      }
      stay()
    case Event(e, s) =>
      log.info("Got something unexpected in stateName ={} with stateData={} from sending={} with message={}",  stateName, stateData, s, e)  
      stay()
  }

  def closeTrade(id: Long, trades: List[Trade], 
      allAveragePrices:ArrayBuffer[PrecisionType], patterns: ArrayBuffer[Pattern], performances: ArrayBuffer[PrecisionType],
      accountInfo : AccountResponse, initialBalance: Double) =
    trades
      .find(trade => trade.id==id)
      .fold(stay())(
        trade => {
          webLog ! TradeClosed(trade.id, trade.instrument)
          val orderToSend = new CancelOrderRequest(id, trade.units, trade.instrument)
          goto(SendingOrder) using OrderToSend(List(orderToSend), List(trade), allAveragePrices, patterns, performances, accountInfo, initialBalance, 0, orderToSend)
        }
      )

  when(SendingOrder) {
    case Event(confirmation: OrderResponseSuccess, ots: OrderToSend) =>
      if(confirmation.i == instrument.desc) {
        if(ots.orderRequests.isEmpty) {
          log.info("Going to BeforeStart got create stateData={}", stateData)
          goto(BeforeStart) using StrategyState(ots.orders, ots.allAveragePrices, ots.patterns, ots.performances, ots.accountInfo, ots.initialBalance) 
        }
        else {
          if(ots.orderRequests.head != ots.lastRequest)
          {
            sendOrder(ots.orderRequests.head)
            stay() using ots.copy(orderRequests = ots.orderRequests.drop(1), lastRequest = ots.orderRequests.head)  
          }
          else
          {
            if(ots.orderRequests.drop(1).isEmpty)
            {
              log.info("Going to BeforeStart got create stateData={}", stateData)
              goto(BeforeStart) using StrategyState(ots.orders, ots.allAveragePrices, ots.patterns, ots.performances, ots.accountInfo, ots.initialBalance)
            }
            else
            {
              sendOrder(ots.orderRequests.drop(1).head)
              stay() using ots.copy(orderRequests = ots.orderRequests.drop(2), lastRequest = ots.orderRequests.drop(1).head) 
            }
          }
        }
      }
      else {
        stay()
      }
    case Event(failure: OrderResponseError, ots: OrderToSend) =>
      if(failure.i == instrument.desc)
      {
        log.info("got a failure - goiing back to before start stateData={}", stateData)
        if(ots.errorCount > 5 || ots.orderRequests.isEmpty || (ots.orderRequests.head == ots.lastRequest && ots.orderRequests.drop(1).isEmpty))
          goto(BeforeStart) using StrategyState(ots.orders, ots.allAveragePrices, ots.patterns, ots.performances, ots.accountInfo, ots.initialBalance)
        else {
          if(ots.orderRequests.head == ots.lastRequest) {
            sendOrder(ots.orderRequests.drop(1).head)
            stay() using ots.copy(orderRequests = ots.orderRequests.drop(2), errorCount = ots.errorCount + 1, lastRequest = ots.orderRequests.drop(1).head)  
          }
          else {
             sendOrder(ots.orderRequests.head)
             stay() using ots.copy(orderRequests = ots.orderRequests.drop(1), errorCount = ots.errorCount + 1, lastRequest = ots.orderRequests.head)
          }
        }
      }
      else {
        stay()
      }
    case Event(account : AccountResponse, data: OrderToSend) =>
      stay() using data.copy(accountInfo = account)
    case Event(tradeResponse : FetchTradeResponse, data: OrderToSend) =>
      reportTradesUpdate(tradeResponse.trades)
      stay() using data.copy(orders = tradeResponse.trades)
    case Event(tick: Tick, otm: OrderToSend) =>
      if(tick.instrument == instrument.toString()) {
        
        otm.allAveragePrices :+ (tick.bid + tick.ask) / 2
        if(otm.errorCount > 5) {
          
          goto(BeforeStart) using StrategyState(otm.orders, otm.allAveragePrices, otm.patterns, otm.performances, otm.accountInfo, otm.initialBalance)  
        }
        else {
          stay()
        }
      }
      else {
        stay()
      }
   case Event(e, s) =>
      log.info("Got something unexpected in stateName ={} with stateData={} from sending={} with message={}",  stateName, stateData, s, e)
      stay()
      
  }

  when(Trading) {
    case Event(tradeResponse : FetchTradeResponse, data:StrategyState) =>
      reportTradesUpdate(tradeResponse.trades)
      stay() using data.copy(orders = tradeResponse.trades)
    case Event(tick: Tick, data: StrategyState) =>
      if(tick.instrument == instrument.toString())
      {
        data.allAveragePrices.append((tick.bid + tick.ask) / 2)
        val limitOfArray = 6000
        
        if(data.patterns.length > limitOfArray)
        {
          data.patterns.remove(0, data.patterns.length - limitOfArray)
        }
        else
        {
          println(instrument.desc + " Not the right now for pattern : " + data.patterns.length);
        }
        
        if(data.performances.length > limitOfArray)
        {
          data.performances.remove(0, data.performances.length - limitOfArray)
        }
        else
        {
          println(instrument.desc + " Not the right now for performance  : " + data.performances.length);
        }
        
        think(data, tick)

      }
      else
      {
        stay()
      }
    case Event(account : AccountResponse, data: StrategyState) =>
      stay() using data.copy(accountInfo = account)
    case Event(CloseTrade(id), data: StrategyState) =>
      closeTrade(id, data.orders, data.allAveragePrices, data.patterns, data.performances, data.accountInfo, data.initialBalance)
  }

  whenUnhandled {
    case Event(e, s) =>
      log.info("Got something unexpected in stateName ={} with stateData={} from sending={} with message={}",  stateName, stateData, s, e)
      
      stay()
  }

  def sendOrder(nextStateData: OrderRequest) = {
    nextStateData match {
      case ots:CreateOrderRequest =>
        log.info("Transition to SendingOrder. createOrderRequest={}", ots)
        orderCreator ! ots
      case otc:CancelOrderRequest =>
        log.info("Transition to SendingOrder. cancelOrderRequest={}", otc)
        orderCanceler ! otc
      case otm:ModifyTradeRequest =>
        log.info("Transition to SendOrder. modifyOrderRequest={}", otm)
        tradeModifier ! otm
      case _ =>
        log.info("Transition to SendingOrder Failed! stateData={} from sending={}",  stateData, sender())
    }
  }
  
  def sendInitialOrderRequest(data: Data)
  {
    data match {
      case ots: OrderToSend =>
        if (ots.orderRequests.isEmpty) {
          log.info("SOMETHING WENT WRONG! NO Orders to send!?????")
        }
        else {
          sendOrder(ots.orderRequests.head)
        }
      case _ =>
        log.warning("Unknow data")
    }
  }

  def reportStateUpdate(state:String, target: ActorRef = webLog) = {
    target ! StateChanged(state)
  }

  onTransition {
    case Trading -> SendingOrder =>
      reportStateUpdate(SendingOrder.toString)
      sendInitialOrderRequest(nextStateData)
    case f -> to =>
      reportStateUpdate(to.toString)
  }

  initialize()

  def think(data: StrategyState, tick : Tick) = {
    val decision = for (
      (newState, newData) <- strategy.think(data, instrument, tick)
    ) yield {
      (newState, newData)
    }
    decision.fold(stay() using data) {
      case (newState, newData) => goto(newState) using newData
    }
  }
}

