package org.akkatrading.live

import java.time.ZonedDateTime

import akka.actor._
import akka.util.Timeout
import org.akkatrading.live.CandleFetcher._
import org.akkatrading.live.util.DateUtils._
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
import org.akkatrading.live.util.Quotes.FullCandle
import org.akkatrading.live.util.TimeFrameEnum.TimeFrameVal
import spray.client.pipelining._
import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.httpx.encoding.{Deflate, Gzip}
import spray.json._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import java.text.SimpleDateFormat
import java.net.URLEncoder

object CandleFetcher {

  def props(hostConnector: ActorRef): Props = Props(new CandleFetcher(hostConnector))

  case class FetchCandles(instrument: InstrumentVal, count: Int, granularity: TimeFrameVal, candleFormat: String, till: Option[ZonedDateTime])

  case class PriceUpdate(instrument: InstrumentVal, granularity: TimeFrameVal, current: Candle, previous: Candle)

  case class BulkPriceUpdate(instrument: InstrumentVal, granularity: TimeFrameVal, candles: List[Candle])

  case class Candle(time: ZonedDateTime, openBid: Double, openAsk: Double, highBid: Double, highAsk: Double, lowBid: Double, lowAsk: Double, closeBid: Double, closeAsk: Double, volume: Int, complete: Boolean) {

    def canEqual(other: Any): Boolean = other.isInstanceOf[Candle] || other.isInstanceOf[FullCandle]

    def equalsPrices(other: Any): Boolean = other match {
      case that: Candle =>
        (that canEqual this) &&
          time == that.time &&
          openBid == that.openBid &&
          openAsk == that.openAsk &&
          highBid == that.highBid &&
          highAsk == that.highAsk &&
          lowBid == that.lowBid &&
          lowAsk == that.lowAsk &&
          closeBid == that.closeBid &&
          closeAsk == that.closeAsk
      case that: FullCandle =>
        (that canEqual this) &&
          time == that.time &&
          openBid == that.openBid &&
          openAsk == that.openAsk &&
          highBid == that.highBid &&
          highAsk == that.highAsk &&
          lowBid == that.lowBid &&
          lowAsk == that.lowAsk &&
          closeBid == that.closeBid &&
          closeAsk == that.closeAsk
      case _ => false
    }

  }

  case class CandleResponse(instrument: String, granularity: String, candles: List[Candle])

  object CandleJsonProtocol extends DefaultJsonProtocol {
    implicit val candleFmt = jsonFormat11(Candle)
    implicit val candleResponseFmt = jsonFormat3(CandleResponse)
  }

}

class CandleFetcher(connector: ActorRef) extends Actor with ActorLogging with AuthInfo {

  import context.dispatcher
  import org.akkatrading.live.CandleFetcher.CandleJsonProtocol._

  implicit val timeout: Timeout = Timeout(15 seconds)

  val pipeline: HttpRequest => Future[CandleResponse] = (
    addCredentials(OAuth2BearerToken(authToken))
      ~> encode(Gzip)
      ~> sendReceive(connector)
      ~> decode(Deflate)
      ~> unmarshal[CandleResponse]
    )
    
  private val iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss'Z'")

  def receive = {
    case FetchCandles(instrument, count, granularity, candleFormat, till) =>
      val target = sender()
      val parameters = Map("instrument" -> instrument.desc,
        "count" -> count.toString(),
        "candleFormat" -> candleFormat,
        "granularity" -> granularity.desc
      )
      val request = Uri("/v1/candles").withQuery(till.fold(parameters) {t =>
        parameters + ("end" -> iso8601Format.format(t.toEpochSecond() * 1000))
      })
      pipeline(Get(request)) onComplete {
        case Success(CandleResponse(_, _, candles)) =>
          log.debug("Fetched candles: {}", candles)
          if (count == 2) {
            target ! PriceUpdate(instrument, granularity, candles(1), candles.head)
          } else {
            log.info(s"Fetched bulk price update for $instrument $granularity: size:${candles.length}")
            target ! BulkPriceUpdate(instrument, granularity, candles)
          }
        case Success(somethingUnexpected) =>
          log.warning("The Oanda API call was successful but returned something unexpected: '{}'.", somethingUnexpected)
        case Failure(error) =>
          log.error(error, "Couldn't fetch candles")
      }
    case other => log.info("Received something unexpected: {}", other)
  }
}
