package org.akkatrading.live

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import org.akkatrading.live.EventListener.SubscribeToEvents
import org.akkatrading.live.PriceListener.SubscribeToRates
import org.akkatrading.live.strategy.BlueiceStrategy
import spray.can.Http
import spray.can.Http.{HostConnectorInfo, HostConnectorSetup}
import scala.concurrent.duration._
import org.akkatrading.live.AccountFetcher.AccountRequest
import org.akkatrading.live.OrderFetcher.FetchOrderRequest

object ScalaMain extends App with AuthInfo {
//scala -classpath akka-trading-assembly-1.0.jar org.akkatrading.live.ScalaMain
  val sys = ActorSystem("oanda-client")

  run(sys.actorOf(Props[LoggingSpy]))(sys)

  def run(spyRef:ActorRef)(implicit system:ActorSystem) = {
    implicit val timeout = Timeout(5 seconds)
    import system.dispatcher

    val setup: HostConnectorSetup = Http.HostConnectorSetup("stream-fxpractice.oanda.com", port = 443, sslEncryption = true)
    val streamingConnectorFuture = for {
      HostConnectorInfo(hostConnector, _) <- IO(Http) ? setup
    } yield hostConnector

    val restConnectorFuture = for {
      HostConnectorInfo(hostConnector, _) <- IO(Http) ? Http.HostConnectorSetup("api-fxpractice.oanda.com", port = 443, sslEncryption = true)
    } yield hostConnector

    val strategyFuture = restConnectorFuture.map(
      restConnector => {
        
//        val orderCreator = system.actorOf(OrderCreator.props(restConnector), "orderCreator")
//        val orderCanceller = system.actorOf(OrderCanceler.props(restConnector), "orderCanceler")
//        val tradeModifier = system.actorOf(TradeModifier.props(restConnector), "tradeModifier")
//        val orderFetcher = system.actorOf(OrderFetcher.props(restConnector), "orderFetcher")
//        val candleFetcher = system.actorOf(CandleFetcher.props(restConnector), "candleFetcher")
        system.actorOf(
          InstrumentDemultiplexer.props(
            instruments.map {
              instrument =>
                instrument -> system.actorOf(
                  StrategyFSM.props(
                    spyRef,
                    instrument,
                    system.actorOf(TradeModifier.props(restConnector), "tradeModifier" + instrument.desc.toString()),
                    system.actorOf(OrderCreator.props(restConnector), "orderCreator" + instrument.desc.toString()),
                    system.actorOf(OrderCanceler.props(restConnector), "orderCanceler"+ instrument.desc.toString()),
                    system.actorOf(CandleFetcher.props(restConnector), "candleFetcher"+ instrument.desc.toString()),
                    new BlueiceStrategy(spyRef)
                  ),
                  "orderManager"+ instrument.desc.toString()
                )
            }.toMap
          )
        )
        
      }
    )
    
    val repeatFuture = restConnectorFuture.map(
      restConnector => {
        val accountFetcher  = system.actorOf(AccountFetcher.props(restConnector, instruments), "accountFetcher")
        val orderFetcher  = system.actorOf(OrderFetcher.props(restConnector, instruments), "orderFetcher")
        system.scheduler.schedule(0 milliseconds, 6000 milliseconds, accountFetcher, AccountRequest)
        system.scheduler.schedule(0 milliseconds, 5000 milliseconds, orderFetcher, FetchOrderRequest(None, None, None, None))
      })
    
    strategyFuture onSuccess {
      case orderManager =>
        streamingConnectorFuture onSuccess {
          case streamingConnector =>
            val eventListener = system.actorOf(EventListener.props(streamingConnector, orderManager), "eventListener")
            eventListener ! SubscribeToEvents
            val priceListener = system.actorOf(PriceListener.props(streamingConnector, orderManager), "priceListener")
            priceListener ! SubscribeToRates()
        }
    }
//    repeatFuture
    strategyFuture
  }

  class LoggingSpy extends Actor with ActorLogging {
    override def receive: Receive = {
      case any:AnyRef => {
//        log.info(s"Event: ${any.toString}}")
      }
    }
  }
}
