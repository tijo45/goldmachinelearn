package org.akkatrading.live

import java.io.PrintWriter
import java.time.ZoneId
import java.text.SimpleDateFormat
import java.io.File
import akka.actor.Actor
import java.util.TimeZone
import org.akkatrading.live.PriceListener.Tick
import akka.actor.Props

object TickWriter {
 
  def props(inst: String): Props = Props(new TickWriter(inst))
}

class TickWriter(inst: String) extends Actor {
  val buffer = scala.collection.mutable.ListBuffer[Tick]()
  val writer: PrintWriter = {
    val startTime = System.currentTimeMillis
    val fileName = s"src/main/resources/ticks_${inst}_${startTime}.csv"
    new PrintWriter(new File(fileName))
//    null
  }
  val dateTimeFormat = {
    val datePattern = "yyyy.MM.dd HH:mm:ss.SSS"
    val dateTimeFormat = new SimpleDateFormat(datePattern)
    dateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    dateTimeFormat
  }
  val zoneId = ZoneId.of("UTC")
  
  def receive = {
    case t: Tick =>
      buffer.append(t)
      if (buffer.size == 100) {
        println("got another 100 for " + inst);
        for (tick <- buffer) {
          val line = List(
            dateTimeFormat.format(tick.time.withZoneSameLocal(zoneId).toEpochSecond() * 1000 + tick.time.getNano / 1000000),
            tick.instrument,
            tick.bid.toString(),
            tick.ask.toString()
          )
          writer.println(line.mkString(";"))
          writer.flush()
        }
        buffer.clear
      }
  }
}
