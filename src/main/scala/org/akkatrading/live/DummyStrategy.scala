package org.akkatrading.live

import akka.actor.{Props, ActorSystem, Actor, ActorRef}
import org.akkatrading.live.StrategyFSM._

import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection._

class DummyStrategy(spy:ActorRef) extends Actor {

  sealed trait Imitation

  case object ImitatePriceChange extends Imitation

  case object ImitateStateChange extends Imitation

  case object ImitateTradeChange extends Imitation

  private val instruments = List("USD_EUR", "GPB_USD", "USD_JPY", "USD_CHF").map(_ + "_")

  private val states = List(BeforeStart, DownloadingMissingCandles, SendingOrder, Trading).map(_ + "_")

  private val sides = List("buy", "cell")

  private val random = new Random()

  context.system.scheduler.schedule(0.seconds, 700.milliseconds, self, ImitatePriceChange)(context.system.dispatcher)
  context.system.scheduler.schedule(0.seconds, 5.seconds, self, ImitateStateChange)(context.system.dispatcher)
  context.system.scheduler.schedule(5.seconds, 20.seconds, self, ImitateTradeChange)(context.system.dispatcher)

  def nextOpt[T](list:List[T]) = list match {
    case Nil => None
    case _ => Some(next(list))
  }

  def next[T](list:List[T]) = list(random.nextInt(list.size))

  val openedTrades = mutable.HashMap.empty[Long, String]

  override def receive: Receive = {
    case ImitatePriceChange =>
      spy ! CurrentPrice(
        next(instruments),
        random.nextDouble()*10.0,
        immutable.Map(
          "adx" -> random.nextDouble()*7.0,
          "adxPrev" -> random.nextDouble()*5.0,
          "plusDI" -> random.nextDouble()*8.0,
          "minusDI" -> random.nextDouble()*8.0,
          "calculatedStopLoss" -> random.nextDouble()*11.0,
          "pipTarget" -> random.nextDouble()*5.0,
          "lastATRHourValue" -> random.nextDouble()*4.0
        )
      )
    case ImitateStateChange =>
      spy ! StateChanged(next(states))
    case ImitateTradeChange =>
      val buf = instruments.toBuffer -- openedTrades.values
      nextOpt(buf.toList).map { instrument =>
        val id = (1000.0*random.nextDouble()).toLong
        openedTrades += (id -> instrument)
        spy ! TradeState(id, instrument, next(sides), random.nextDouble()*10.0)
      }
    case CloseTrade(id) =>
      openedTrades.get(id).map { instrument =>
        openedTrades -= id
        spy ! TradeClosed(id, instrument)
      }
  }
}

object DummyStrategy {

  def run(spyRef:ActorRef)(implicit system:ActorSystem) = {
    Future(system.actorOf(Props(new DummyStrategy(spyRef)), "dummyStrategy"))
  }
}
