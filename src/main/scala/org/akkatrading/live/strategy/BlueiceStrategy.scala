package org.akkatrading.live.strategy

import akka.actor.ActorRef
import org.akkatrading.live.CurrentPrice
import org.akkatrading.live.OrderCreator.CreateOrderRequest
import org.akkatrading.live.TradeModifier.ModifyTradeRequest
import org.akkatrading.live.StrategyFSM.{OrderRequest, SendingOrder, StrategyState, OrderToSend}
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
import org.akkatrading.live.util.{OrderType, Side, TimeFrameImmutable}
import org.goldmine.functions.{ADXFunction, ATRFunction}
import org.goldmine.indicator.Factor
import org.slf4j.LoggerFactory
import org.akkatrading.live.StrategyFSM.OrderRequest
import org.akkatrading.live.OrderCanceler.CancelOrderRequest
import java.time.ZonedDateTime
import org.goldmine.functions.PatternProcessor
import org.akkatrading.live.PriceListener.Tick

class BlueiceStrategy(webLog: ActorRef) extends Strategy {

  private[this] val log = LoggerFactory.getLogger(this.getClass)

  val periodDi = new Factor("Period Di", 14)
  val periodAdx = new Factor("Period Adx", 14)
  val periodAtr = new Factor("Period Atr", 14)

  implicit def ordered: Ordering[ZonedDateTime] = new Ordering[ZonedDateTime] {
    def compare(x: ZonedDateTime, y: ZonedDateTime): Int = y compareTo x
  }

  def think(data: StrategyState, instrument:InstrumentVal, tick : Tick) = {
    val start = System.nanoTime()
    
    
    var returnvalue : List[OrderRequest] = List.empty
    
    
    if(data.allAveragePrices.length > 40 )
    {
      val totalStartTimeMillis = System.currentTimeMillis()
      PatternProcessor.storePatterns(data.allAveragePrices.toArray, data.patterns, data.performances)
      val ticks = PatternProcessor.buildTicks(data.allAveragePrices.toArray)
      val possibleOutCome = PatternProcessor.matchPatterns(ticks, data.patterns, data.performances)
      
      def orderOption = data.orders.filter(o=>o.instrument == instrument.toString()).sortBy { x => x.id }   
      def buyOrderOption = data.orders.filter(o=>o.instrument == instrument.toString() && o.side == Side.buy.toString()).sortBy { x => x.id }
      def sellOrderOption = data.orders.filter(o=>o.instrument == instrument.toString() && o.side == Side.sell.toString()).sortBy { x => x.id }
      val unitToPipFactor = 10000
       if(!buyOrderOption.isEmpty || !sellOrderOption.isEmpty) {  
        var returnvalue : List[OrderRequest] = List.empty
        buyOrderOption.foreach { buy =>         
          println(instrument.desc + " : BUY :  "+ ((tick.bid - buy.price) * unitToPipFactor))
          if ((tick.bid - buy.price) * unitToPipFactor > 5) {
            returnvalue = returnvalue :+ CancelOrderRequest(buy.id, buy.units, buy.instrument)
          }
          if (Math.abs((tick.bid - buy.price) * unitToPipFactor) > 35) {
            returnvalue = returnvalue :+ CancelOrderRequest(buy.id, buy.units, buy.instrument)
          }
        }
        sellOrderOption.foreach { sell => 
          println(instrument.desc + " : SELL :  "+ ((sell.price - tick.ask) * unitToPipFactor))
          if ((sell.price - tick.ask) * unitToPipFactor > 3) {
            returnvalue = returnvalue  :+ CancelOrderRequest(sell.id, sell.units, sell.instrument)          
          }
          if (Math.abs((sell.price - tick.ask) * unitToPipFactor) > 35) {
            returnvalue = returnvalue  :+ CancelOrderRequest(sell.id, sell.units, sell.instrument)          
          }
        }
        if(returnvalue.isEmpty)
          None
        else {
          returnvalue.foreach { x => 
            println(instrument.desc +  " ORDER NUMBER " + x.orderId + " for : "+ x.i + " type : " + x.getClass)
          }          
          Option(SendingOrder, OrderToSend(returnvalue.sortBy { x => x.orderId}, data.orders, data.allAveragePrices, data.patterns, data.performances, data.accountInfo, data.initialBalance, 0, returnvalue.head))
        }
      }  
      else if(((Math.abs(tick.bid - tick.ask) * 10000) < 5.0))
        None
      else if (possibleOutCome < 0 && (orderOption.size  < 100)) {
        //sell
        println("Starting to sell")
        val createOrderRequest = CreateOrderRequest(instrument, 100, Side.sell, OrderType.market, Option(50) ,None, None, 
              None, None) 
        Option(SendingOrder, OrderToSend(List(createOrderRequest), data.orders, data.allAveragePrices, data.patterns, data.performances, data.accountInfo, data.initialBalance, 0, createOrderRequest))
      }
      else if (possibleOutCome > 0 && (orderOption.size  < 100)) {
        val createOrderRequest = CreateOrderRequest(instrument, 100, Side.buy, OrderType.market, Option(50), None, None, None, None)
        println(instrument.desc +  " Starting to buy")
        Option(SendingOrder, OrderToSend(List(createOrderRequest), data.orders, data.allAveragePrices, data.patterns, data.performances, data.accountInfo, data.initialBalance, 0, createOrderRequest))
      }
      else
        None
    }
    else{
      println(instrument.desc + data.allAveragePrices.size)
      None
    }
      
  }
  
  def getRollingProfit(initialBalance : Double , currentBalance: Double) : Double = {
    (currentBalance - initialBalance) / 2.0  
  }
}
