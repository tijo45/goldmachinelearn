package org.akkatrading.live.strategy

import org.akkatrading.live.StrategyFSM.{Data, State, StrategyState}
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
import org.akkatrading.live.util.TimeFrameImmutable
import akka.actor.ActorRef
import org.akkatrading.live.PriceListener.Tick
import org.akkatrading.live.StrategyFSM.PrecisionType

trait Strategy {

  /**
    * Makes the decision what to do next. Returns Some of new state and new data if state change required, None otherwise
    */
  def think(data: StrategyState, instrument:InstrumentVal, tick: Tick):Option[(State, Data)]
}
