package org.akkatrading.live

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.akkatrading.live.util.InstrumentEnum.InstrumentVal

case class MultiplexedSignal(instrument:InstrumentVal, signal: AnyRef)

object InstrumentDemultiplexer {
  def props(receivers:Map[InstrumentVal, ActorRef]) = Props(new InstrumentDemultiplexer(receivers))
}

class InstrumentDemultiplexer(receivers:Map[InstrumentVal, ActorRef]) extends Actor with ActorLogging {

  override def receive = {
    case MultiplexedSignal(instrument, signal) =>
      receivers.get(instrument).map (_ ! signal)
    case _@signal => receivers.values.foreach (_ ! signal)
  }
}
