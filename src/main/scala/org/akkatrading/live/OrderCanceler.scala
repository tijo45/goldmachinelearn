package org.akkatrading.live

import java.time.ZonedDateTime
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import org.akkatrading.live.OrderCanceler.{CancelOrderConfirmation, CancelOrderRequest}
import org.akkatrading.live.util.DateUtils._
import spray.client.pipelining._
import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.json.DefaultJsonProtocol
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import org.akkatrading.live.StrategyFSM.OrderRequest
import org.akkatrading.live.StrategyFSM.OrderResponseSuccess
import org.akkatrading.live.StrategyFSM.OrderResponseError

object OrderCanceler {

  def props(connector: ActorRef): Props = Props(new OrderCanceler(connector))

  case class CancelOrderRequest(id: Long, units: Int, instrument: String) 
    extends OrderRequest(instrument, id)

  case class CancelOrderConfirmation(id: Long, instrument: String, side: String, price: Double, profit: Double, time: ZonedDateTime) 
    extends OrderResponseSuccess(instrument)
  
  case class CancelOrderFailure(instrument: String, error : String) extends OrderResponseError(instrument, error) 
  
  object CancelOrderJsonProtocol extends DefaultJsonProtocol {
    implicit val cancelOrderConfirmationFormat = jsonFormat6(CancelOrderConfirmation)
  }

}

class OrderCanceler(connector: ActorRef) extends Actor with ActorLogging with AuthInfo {

  import context.dispatcher
  import org.akkatrading.live.OrderCanceler.CancelOrderJsonProtocol._
  import org.akkatrading.live.OrderCanceler.CancelOrderFailure

  implicit val timeout = Timeout(5 seconds)

  val pipeline = addCredentials(OAuth2BearerToken(authToken)) ~> sendReceive(connector) ~> unmarshal[CancelOrderConfirmation]

  override def receive = {
    case request: CancelOrderRequest => handleRequest(sender(), request)
  }

  def handleRequest(sender: ActorRef, orderRequest: CancelOrderRequest) = {
    val s = sender
    
    pipeline(Delete(s"/v1/accounts/$accountId/trades/${orderRequest.id}?units=${orderRequest.units}")) onComplete {
      case Success(conf: CancelOrderConfirmation) =>
        log.info("Limit Order canceled: {}", conf)
        s ! conf
      case Success(somethingUnexpected) =>
        log.warning("The Oanda API call was successful but returned something unexpected: '{}'.", somethingUnexpected)
        s ! CancelOrderFailure(orderRequest.instrument, somethingUnexpected.toString())
      case Failure(error) =>
        log.error(error, "Couldn't cancel order" + orderRequest.orderId + " : " + orderRequest.instrument)
        s ! CancelOrderFailure(orderRequest.instrument, error.toString())
    }
  }
}
