//package org.akkatrading.live
//
//import akka.actor.ActorSystem
//import akka.testkit.{ImplicitSender, TestFSMRef, TestKit, TestProbe}
//import org.akkatrading.live.CandleFetcher.{FetchCandles, PriceUpdate}
//import org.akkatrading.live.OrderFetcher.FetchOrderRequest
//import org.akkatrading.live.StrategyFSM.{BeforeStart, DownloadingMissingCandles, StrategyState, Trading}
//import org.akkatrading.live.strategy.Strategy
//import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
//import org.akkatrading.live.util.{InstrumentEnum, TimeFrameEnum, TimeFrames}
//import org.scalamock.scalatest.MockFactory
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
//import org.akkatrading.live.AccountFetcher.AccountResponse
//
//class StrategyFSMTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
//  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory with TestUtils {
//
//  "StrategyFSM" must {
//    val webLog = TestProbe()
//    val orderCreator = TestProbe()
//    val orderCanceler = TestProbe()
//    val candleFetcher = TestProbe()
//    val instrument: InstrumentVal = InstrumentEnum.EUR_USD
//    val fsm = TestFSMRef(new StrategyFSM(webLog.ref, instrument, TestProbe().ref, orderCreator.ref, orderCanceler.ref, candleFetcher.ref, stub[Strategy]))
//    "start in BeforeStart state" in {
//      fsm.stateName should be (BeforeStart)
//    }
//  }
//
//  "BeforeStart" must {
//    val webLog = TestProbe()
//    val orderCreator = TestProbe()
//    val orderCanceler = TestProbe()
//    val candleFetcher = TestProbe()
//    val orderFetcher = TestProbe()
//    val instrument: InstrumentVal = InstrumentEnum.EUR_USD
//    val fsm = TestFSMRef(new StrategyFSM(webLog.ref, instrument,  TestProbe().ref, orderCreator.ref, orderCanceler.ref, candleFetcher.ref, stub[Strategy], false))
//    "ignore different instrument price updates" in {
//      fsm ! PriceUpdate(InstrumentEnum.EUR_CAD, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      fsm.stateName should be (BeforeStart)
//    }
//    "send FetchOrderRequest in BeforeState when not enough candles" in {
//      fsm ! PriceUpdate(instrument, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      candleFetcher.expectMsg(FetchCandles(instrument, 50, TimeFrameEnum.M1, "bidask"))
//      fsm.stateName should be (DownloadingMissingCandles)
//    }
//    "transition to trading from BeforeState when enough candles" in {
//      class TimeFramesStub extends TimeFrames(null)
//      val timeFrames = stub[TimeFramesStub]
//      timeFrames.handlePriceUpdate _ when * returning timeFrames
//      timeFrames.enoughCandles _ when * returning true
//      fsm.setState(BeforeStart, StrategyState(List(), timeFrames, new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0))
//      fsm ! PriceUpdate(instrument, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      fsm.stateName should be (Trading)
//    }
//    "FetchTradeResponse" in {
////      class TimeFramesStub extends TimeFrames(null)
////      val timeFrames = stub[TimeFramesStub]
////      timeFrames.handlePriceUpdate _ when * returning timeFrames
////      timeFrames.enoughCandles _ when * returning true
//      fsm.setState(BeforeStart)
//
//      val item = tradeRandom()
//      fsm ! OrderFetcher.FetchTradeResponse(List(item))
////      webLog.expectMsg(StateChanged(BeforeStart.toString))
////      webLog.expectMsg(StateChanged(BeforeStart.toString))
////      webLog.expectMsg(TradeState(item.id, item.instrument, item.side, item.price))
//      fsm.stateName should be (BeforeStart)
//      fsm.stateData.asInstanceOf[StrategyState].orders should be (List(item))
//      fsm.setState(BeforeStart)
//      
//      val item2 = tradeRandom()
//      val item3 = tradeRandom()
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      fsm ! OrderFetcher.FetchTradeResponse(List(item2, item3))
//      
//      fsm.stateName should be (BeforeStart)
//      fsm.stateData.asInstanceOf[StrategyState].orders should be (List(item2, item3))
//      
//    }
//    "Ignore unknown message" in {
//      fsm.setState(BeforeStart)
//      fsm ! "Unknown"
//      fsm.stateName should be (BeforeStart)
//    }
//  }
//
//  "DownloadingMissingCandles state" should {
//    val webLog = TestProbe()
//    val orderCreator = TestProbe()
//    val orderCanceler = TestProbe()
//    val candleFetcher = TestProbe()
//    val orderFetcher = TestProbe()
//    val instrument: InstrumentVal = InstrumentEnum.EUR_USD
//    val fsm = TestFSMRef(new StrategyFSM(webLog.ref, instrument, TestProbe().ref, orderCreator.ref, orderCanceler.ref, candleFetcher.ref, stub[Strategy], false))
////    "Handle bulk price update" in {
////      fsm.setState(DownloadingMissingCandles)
////      val c = candle()
////      fsm ! BulkPriceUpdate(instrument, TimeFrameEnum.M1, List(c))
////      fsm.stateData.asInstanceOf[StrategyState].tfs(TimeFrameEnum.M1).get.candles should be (List(FullCandle(c)))
////      fsm.stateName should be (BeforeStart)
////    }
//    "Ignore all other messages" in {
//      fsm.setState(DownloadingMissingCandles)
//      fsm ! "Unknown"
//      fsm.stateName should be (DownloadingMissingCandles)
//    }
//  }
//
//  "Trading state" should {
//    val webLog = TestProbe()
//    val orderCreator = TestProbe()
//    val orderCanceler = TestProbe()
//    val candleFetcher = TestProbe()
//    val orderFetcher = TestProbe()
//    val instrument: InstrumentVal = InstrumentEnum.EUR_USD
//    val fsm = TestFSMRef(new StrategyFSM(webLog.ref, instrument, TestProbe().ref, orderCreator.ref, orderCanceler.ref, candleFetcher.ref, stub[Strategy], false))
////    "Handle bulk price update" in {
////      fsm.setState(Trading)
////      val c = candle()
////      fsm ! BulkPriceUpdate(instrument, TimeFrameEnum.M1, List(c))
////      fsm.stateData.asInstanceOf[StrategyState].tfs(TimeFrameEnum.M1).get.candles should be (List(FullCandle(c)))
////      fsm.stateName should be (Trading)
////    }
//    "Ignore all other messages" in {
//      fsm.setState(Trading)
//      fsm ! "Unknown"
//      fsm.stateName should be (Trading)
//    }
//  }
//}
