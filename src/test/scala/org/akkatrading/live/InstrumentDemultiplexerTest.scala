package org.akkatrading.live

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.akkatrading.live.util.InstrumentEnum
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class InstrumentDemultiplexerTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }

  "InstrumentDemultiplexer" must {
    "Forward message to actor" in {
      val usdEurEcho = TestProbe()
      val gpbUsdEcho = TestProbe()
      val fsm = TestActorRef(new InstrumentDemultiplexer(Map(InstrumentEnum.EUR_USD -> usdEurEcho.ref, InstrumentEnum.GBP_USD -> gpbUsdEcho.ref)))
      fsm ! MultiplexedSignal(InstrumentEnum.EUR_USD, "test")
      usdEurEcho.expectMsg("test")
      gpbUsdEcho.expectNoMsg()
      fsm ! MultiplexedSignal(InstrumentEnum.GBP_USD, "test2")
      usdEurEcho.expectNoMsg()
      gpbUsdEcho.expectMsg("test2")
    }
    "Forward message to all actors if no addres specified" in {
      val usdEurEcho = TestProbe()
      val gpbUsdEcho = TestProbe()
      val fsm = TestActorRef(new InstrumentDemultiplexer(Map(InstrumentEnum.EUR_USD -> usdEurEcho.ref, InstrumentEnum.GBP_USD -> gpbUsdEcho.ref)))
      fsm ! "test3"
      usdEurEcho.expectMsg("test3")
      gpbUsdEcho.expectMsg("test3")
    }
    "Swallow message to unknown actor" in {
      val usdEurEcho = TestProbe()
      val fsm = TestActorRef(new InstrumentDemultiplexer(Map(InstrumentEnum.EUR_USD -> usdEurEcho.ref)))
      fsm ! MultiplexedSignal(InstrumentEnum.GBP_USD, "test4")
      usdEurEcho.expectNoMsg()
    }
  }
}