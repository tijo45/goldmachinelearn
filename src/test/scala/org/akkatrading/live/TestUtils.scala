//package org.akkatrading.live
//
//import java.time.ZonedDateTime
//import org.akkatrading.live.CandleFetcher.Candle
//import org.akkatrading.live.OrderCanceler.{CancelOrderConfirmation, CancelOrderRequest}
//import org.akkatrading.live.OrderCreator.{CreateOrderConfirmation, CreateOrderRequest, TradeOpened}
//import org.akkatrading.live.OrderFetcher.Trade
//import org.akkatrading.live.PriceListener.Tick
//import org.akkatrading.live.StrategyFSM.{OrderToClose, OrderToSend, OrderToModify}
//import org.akkatrading.live.TradeModifier._
//import org.akkatrading.live.util.{InstrumentEnum, TimeFrames}
//import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
//import scala.util.Random
//import org.akkatrading.live.AccountFetcher.AccountResponse
//
//trait TestUtils {
//  val random = new Random()
//
//  def randomItem[T](list:List[T], default: T) = list match {
//    case Nil => default
//    case _ => list(random.nextInt(list.size))
//  }
//
//  def randomInstrument() = randomItem(List(InstrumentEnum.EUR_CAD, InstrumentEnum.EUR_USD, InstrumentEnum.GBP_USD), InstrumentEnum.EUR_USD)
//
//  def candleRandom(): Candle = Candle(
//    ZonedDateTime.now(), random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble(),
//    random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextInt(), random.nextBoolean()
//  )
//
//  def tradeRandom(instrument:InstrumentVal = InstrumentEnum.EUR_USD) =
//    Trade(random.nextLong(), instrument.toString(), random.nextInt(10), random.nextString(3), ZonedDateTime.now(), random.nextDouble(),
//      random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble()
//    )
//
//  def tradeOpenedRandom() = TradeOpened(random.nextLong(), random.nextInt(), random.nextString(3), random.nextDouble(), random.nextDouble(), random.nextDouble())
//
//  def createOrderRequestRandom(instrument:InstrumentVal = randomInstrument()) = CreateOrderRequest(instrument, random.nextInt(), random.nextString(2), random.nextString(2), None, None, None, None)
//
//  def orderToSendRandom(instrument:InstrumentVal = randomInstrument(), tickCount:Int = random.nextInt()) = OrderToSend(createOrderRequestRandom(instrument), List(), new TimeFrames(Map()), new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0, tickCount)
//
//  def createOrderConfirmationRandom(instrument:InstrumentVal = randomInstrument()) = CreateOrderConfirmation(instrument.desc, ZonedDateTime.now(), random.nextDouble(), tradeOpenedRandom())
//
//  def cancelOrderRequestRandom(instrument:InstrumentVal = randomInstrument()) = CancelOrderRequest(random.nextInt(), random.nextInt(10), instrument)
//
//  def modifyOrderRequestRandom(instrument:InstrumentVal = randomInstrument()) = ModifyTradeRequest(random.nextInt(), instrument, Option(random.nextDouble))
//
//  def orderToCloseRandom(instrument:InstrumentVal = randomInstrument(), tickCount:Int = random.nextInt()) = OrderToClose(cancelOrderRequestRandom(instrument), List(), new TimeFrames(Map()), new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0, tickCount)
//
//  def orderToModifyRandom(instrument:InstrumentVal = randomInstrument(), tickCount:Int = random.nextInt()) = OrderToModify(modifyOrderRequestRandom(instrument), List(), new TimeFrames(Map()), new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0, tickCount)
//
//  def cancelOrderConfirmationRandom(instrument:InstrumentVal = randomInstrument()) = CancelOrderConfirmation(random.nextLong(), instrument.desc, random.nextString(3), random.nextDouble(), random.nextDouble(), ZonedDateTime.now())
//
//  def tickRandom(instrument:InstrumentVal = randomInstrument()) = Tick(instrument, ZonedDateTime.now(), random.nextDouble(), random.nextDouble())
//}
