//package org.akkatrading.live
//
//import akka.actor.ActorSystem
//import akka.testkit.{ImplicitSender, TestFSMRef, TestKit, TestProbe}
//import org.akkatrading.live.OrderFetcher.FetchOrderRequest
//import org.akkatrading.live.StrategyFSM._
//import org.akkatrading.live.strategy.Strategy
//import org.akkatrading.live.util.InstrumentEnum
//import org.scalamock.scalatest.MockFactory
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
//
//class SendingOrderStateTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
//  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory with TestUtils {
//
//  "CreateOrderConfirmation event" should {
//    "be ignored for different instrument" in {
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_CAD))
//      fsm ! createOrderConfirmationRandom()
//      fsm.stateName should be (SendingOrder)
//    }
//    "move to BeforeStart for appropriate instrument" in {
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref,  stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_USD))
//      fsm ! createOrderConfirmationRandom()
//      fsm.stateName should be (BeforeStart)
//    }
//    "send nothing to fetchers for different instrument" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD,  TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_CAD))
//      fsm ! createOrderConfirmationRandom()
//      probe.expectNoMsg()
//    }
//  }
//
//  "CancelOrderConfirmation event" should {
//    "be ignored for different instrument" in {
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_CAD))
//      fsm ! cancelOrderConfirmationRandom()
//      fsm.stateName should be (SendingOrder)
//    }
//    "move to BeforeStart for appropriate instrument" in {
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_USD))
//      fsm ! cancelOrderConfirmationRandom()
//      fsm.stateName should be (BeforeStart)
//    }
//    "send nothing to fetchers for different instrument" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD,  TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_CAD))
//      fsm ! cancelOrderConfirmationRandom()
//      probe.expectNoMsg()
//    }
//  }
//
//  "(OrderForSend)Tick event" should {
//    "be ignored for different instrument" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_USD))
//      fsm ! tickRandom(InstrumentEnum.EUR_CAD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "stay in SendingOrder for appropriate instrument when not enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_USD, tickCount = 10))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "move to Trading for appropriate instrument when enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToSendRandom(InstrumentEnum.EUR_USD, tickCount = 11))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (Trading)
//      probe.expectNoMsg()
//    }
//  }
//
//  "(OrderToClose)Tick event" should {
//    "be ignored for different instrument" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref,  stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_USD))
//      fsm ! tickRandom(InstrumentEnum.EUR_CAD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "stay in SendingOrder for appropriate instrument when not enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_USD, tickCount = 10))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "move to Trading for appropriate instrument when enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToCloseRandom(InstrumentEnum.EUR_USD, tickCount = 11))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (Trading)
//      probe.expectNoMsg()
//    }
//  }
//
//
//
//
//"(OrderToModify)Tick event" should {
//    "be ignored for different instrument" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref,  stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToModifyRandom(InstrumentEnum.EUR_USD))
//      fsm ! tickRandom(InstrumentEnum.EUR_CAD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "stay in SendingOrder for appropriate instrument when not enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToModifyRandom(InstrumentEnum.EUR_USD, tickCount = 10))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//    "move to Trading for appropriate instrument when enough TicksCount" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder, orderToModifyRandom(InstrumentEnum.EUR_USD, tickCount = 11))
//      fsm ! tickRandom(InstrumentEnum.EUR_USD)
//      fsm.stateName should be (Trading)
//      probe.expectNoMsg()
//    }
//  }
//
//
//
//
//
//
//
//
//
//
//  
//
//  "Unhandled " should {
//    "be ignored for now" in {
//      val probe = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, probe.ref, probe.ref, probe.ref, stub[Strategy], false))
//      fsm.setState(SendingOrder)
//      fsm ! "Unknown event"
//      fsm.stateName should be (SendingOrder)
//      probe.expectNoMsg()
//    }
//  }
//}
