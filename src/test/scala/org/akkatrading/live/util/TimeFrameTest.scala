//package org.akkatrading.live.util
//
//import java.time.ZonedDateTime
//import java.time.temporal.ChronoUnit
//
//import org.akkatrading.live.CandleFetcher.{PriceUpdate, Candle}
//import org.akkatrading.live.PriceListener.Tick
//import org.akkatrading.live.util.InstrumentEnum.InstrumentVal
//import org.akkatrading.live.util.Quotes.FullCandle
//import org.akkatrading.live.util.TimeFrameEnum.TimeFrameVal
//import org.specs2._
//
//import scala.collection.mutable.ArrayBuffer
//
//class TimeFrameTest extends mutable.Specification {
//
//  def candle(time:ZonedDateTime, volume:Int = 1) = Candle(time, 0, 0, 0, 0, 0, 0, 0, 0, volume, complete = false)
//
//  def fullCandle(time:ZonedDateTime, volume:Int = 1) = FullCandle(candle(time,volume))
//
//  def generateCandles(list:List[Long], base:ZonedDateTime) = list.map(k => fullCandle(base.minusMinutes(k)))
//
//  "Empty TimeFrame" should {
//    "Return empty slice" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.getSlice(3) === ArrayBuffer()
//      tf.getSlice(10) === ArrayBuffer()
//    }
//    "Is consistent" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.checkConsistency() === true
//    }
//    "Enough candles" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.enoughCandles(0) === true
//    }
//    "Not enough candles" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.enoughCandles(1) === false
//      tf.enoughCandles(3) === false
//    }
//    "Bulk updates price" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val c = fullCandle(ZonedDateTime.now().truncatedTo(ChronoUnit.MINUTES))
//      tf.handleBulkPriceUpdate(List(c))
//      tf.lastCandle === c
//    }
//    "Handles tick" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleTick(Tick("TE_ST", ZonedDateTime.now(), 0, 0))
//      tf.enoughCandles(1) === true
//    }
//  }
//
//  "Bulk update" should {
//    val now = ZonedDateTime.now().truncatedTo(ChronoUnit.MINUTES)
//    "Insert update at correct place" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleBulkPriceUpdate(generateCandles(List(10L), now))
//      tf.getSlice(10).map(_.time).toList === List(10L).map(now.minusMinutes)
//      tf.handleBulkPriceUpdate(generateCandles(List(15L, 5L), now))
//      tf.getSlice(10).map(_.time).toList === List(15L, 10L, 5L).map(now.minusMinutes)
//      tf.handleBulkPriceUpdate(generateCandles(List(13L, 7L), now))
//      tf.getSlice(10).map(_.time).toList === List(15L, 13L, 10L, 7L, 5L).map(now.minusMinutes)
//    }
//    "Replace candle at correct place" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleBulkPriceUpdate(List(fullCandle(now.minusMinutes(5L), 2), fullCandle(now.minusMinutes(3L), 3)))
//      val slice = tf.getSlice(10).toList
//      slice.map(_.time) === List(5L, 3L).map(now.minusMinutes)
//      slice.head.time === now.minusMinutes(5L)
//      slice.head.volume === 2
//      tf.handleBulkPriceUpdate(List(fullCandle(now.minusMinutes(5L), 137)))
//      val updated = tf.getSlice(10).toList
//      updated.map(_.time) === List(5L, 3L).map(now.minusMinutes)
//      updated.head.time === now.minusMinutes(5L)
//      updated.head.volume === 137
//    }
//    "Detects consistency" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleBulkPriceUpdate(generateCandles(List(11L, 10L, 9L, 8L, 7L), now)) === true
//      tf.handleBulkPriceUpdate(generateCandles(List(6L, 5L, 4L, 3L, 2L), now)) === true
//    }
//    "Detects inconsistency" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleBulkPriceUpdate(generateCandles(List(11L, 10L, 9L, 8L, 7L), now))
//      tf.handleBulkPriceUpdate(generateCandles(List(5L, 4L, 3L, 2L), now)) === false
//    }
//    "Updates by unsorted list of candles" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      tf.handleBulkPriceUpdate(generateCandles(List(8L, 7L, 11L, 9L, 10L, 10L, 8L), now))
//      tf.getSlice(10).map(_.time).toList === List(11L, 10L, 9L, 8L, 7L).map(now.minusMinutes)
//    }
//  }
//  
//  "Price update" should {
//    val now = ZonedDateTime.now().truncatedTo(ChronoUnit.MINUTES)
//    "Creates two entries for empty quotes" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val previous = candle(now.minusMinutes(1), 71)
//      val current = candle(now, 4)
//      val update = PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current, previous)
//      tf.handlePriceUpdate(update)
//      tf.getSlice(10).map(_.volume).toList === List(71, 4)
//    }
//    "Fail to handle inconsistent price ??? - this looks strange" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val previous = candle(now.minusMinutes(4), 7)
//      val current = candle(now.minusMinutes(3), 41)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current, previous))
//      val previous2 = candle(now.minusMinutes(1), 5)
//      val current2 = candle(now, 11)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current2, previous2))
//      tf.getSlice(10).map(_.volume).toList === List(7, 41)
//    }
//    "Ignore price of previous period" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val previous = candle(now.minusMinutes(4), 7)
//      val current = candle(now.minusMinutes(3), 41)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current, previous))
//      val previous2 = candle(now.minusMinutes(5), 5)
//      val current2 = candle(now.minusMinutes(4), 11)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current2, previous2))
//      tf.getSlice(10).map(_.volume).toList === List(7, 41)
//    }
//    "Append price of consistent update" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val previous = candle(now.minusMinutes(4), 7)
//      val current = candle(now.minusMinutes(3), 41)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current, previous))
//      val current2 = candle(now.minusMinutes(2), 11)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current2, current))
//      tf.getSlice(10).map(_.volume).toList === List(7, 41, 11)
//    }
//    "Update last but one price ??? - this looks strange" in {
//      val tf = new TimeFrame(TimeFrameVal("test", 1))
//      val previous = candle(now.minusMinutes(4), 7)
//      val current = candle(now.minusMinutes(3), 41)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current, previous))
//      val previous2 = candle(now.minusMinutes(4), 19)
//      val current2 = candle(now.minusMinutes(3), 34)
//      tf.handlePriceUpdate(PriceUpdate(InstrumentVal(""), TimeFrameVal("", 1), current2, previous2))
//      tf.getSlice(10).map(_.volume).toList === List(19, 41)
//    }
//  }
//
//  "Handle tick" should {
//    val now = ZonedDateTime.now().truncatedTo(ChronoUnit.MINUTES)
//    "Create candle when quotes empty" in {
//      val tf = new TimeFrame(TimeFrameVal("", 1))
//      tf.handleTick(Tick("", now, 1.0, 2.0))
//      tf.getSlice(10).map(_.volume).toList === List(1)
//    }
//    "Update last price volume if tick within minute" in {
//      val tf = new TimeFrame(TimeFrameVal("", 1))
//      tf.handleTick(Tick("", now.minusMinutes(1), 1.0, 2.0))
//      tf.handleTick(Tick("", now, 4.0, 5.0))
//      tf.getSlice(10).map(_.volume).toList === List(1, 1)
//      tf.handleTick(Tick("", now.plusSeconds(29), 7.0, 9.0))
//      tf.getSlice(10).map(_.volume).toList === List(1, 2)
//      tf.handleTick(Tick("", now.plusSeconds(49), 4.0, 5.0))
//      tf.getSlice(10).map(_.volume).toList === List(1, 3)
//      tf.handleTick(Tick("", now.plusSeconds(59), 8.0, 9.0))
//      tf.getSlice(10).map(_.volume).toList === List(1, 4)
//      tf.handleTick(Tick("", now.plusSeconds(150), 18.0, 9.0))
//      tf.getSlice(10).map(_.volume).toList === List(1, 4)
//    }
//    "Complete last candle and append new" in {
//      val period = 2
//      val tf = new TimeFrame(TimeFrameVal("", period))
//      val start = now.withMinute(3)
//      tf.handleTick(Tick("", start.plusMinutes(period), 1.0, 2.0))
//      tf.handleTick(Tick("", start.plusMinutes(2*period), 2.0, 3.0))
//      tf.handleTick(Tick("", start.plusMinutes(3*period), 4.0, 5.0))
//      val slice = tf.getSlice(10)
//      slice(1).complete === true
//      slice(2).open === (4.0+5.0)/2.0
//    }
//    "Complete candle and append new for crossing hour start period !!! Buggy implementation: In spite of time frame size of 5 minutes, handle tick closes interval of length 1" in {
//      val tf = new TimeFrame(TimeFrameVal("", 5))
//      val prev = now.withMinute(59)
//      tf.handleTick(Tick("", prev, 1.0, 2.0))
//      tf.handleTick(Tick("", prev.plusMinutes(1), 3.0, 4.0))
//      val slice = tf.getSlice(10)
//      slice(0).complete === true
//      slice(1).open === (3.0+4.0)/2.0
//    }
//    "Complete candle and append new for crossing hour start period !!! Buggy implementation: handleTick closes incorrect interval" in {
//      val tf = new TimeFrame(TimeFrameVal("", 1))
//      val prev = now.withHour(3).withMinute(59)
//      val cur = now.withHour(2).withMinute(0)
//      tf.handleTick(Tick("", prev, 1.0, 2.0))
//      tf.handleTick(Tick("", cur, 3.0, 4.0))
//      val slice = tf.getSlice(10)
//      slice(0).complete === true
//      slice(1).open === (3.0+4.0)/2.0
//    }
//    "Complete candle and append new for crossing hour start period !!! Buggy implementation: handleTick failed to cross the hour beginning" in {
//      val interval = 5
//      val tf = new TimeFrame(TimeFrameVal("", 5))
//      val prev = now.withHour(21).withMinute(57)
//      val cur = prev.plusMinutes(interval)
//      tf.handleTick(Tick("", prev, 1.0, 2.0))
//      tf.handleTick(Tick("", cur, 3.0, 4.0))
//      val slice = tf.getSlice(10)
//      slice(0).complete === false
//      slice(0).open === (1.0+2.0)/2.0
//      //Should be:
//      //slice(0).close === true
//      //slice(1).open === (3.0+4.0)/2.0
//    }
//    "Ignore candle ??? " in {
//      val tf = new TimeFrame(TimeFrameVal("", 1))
//      tf.handleTick(Tick("", now, 1.0, 3.0))
//      tf.handleTick(Tick("", now.plusMinutes(3), 4.0, 6.0))
//      tf.getSlice(10).toList.map(_.open) === List(2.00)
//    }
//  }
//}
