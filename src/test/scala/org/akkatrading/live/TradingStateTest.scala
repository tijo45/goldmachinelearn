//package org.akkatrading.live
//
//import akka.actor.ActorSystem
//import akka.testkit.{ImplicitSender, TestFSMRef, TestKit, TestProbe}
//import org.akkatrading.live.OrderFetcher.FetchTradeResponse
//import org.akkatrading.live.StrategyFSM.{StrategyState, Trading}
//import org.akkatrading.live.strategy.Strategy
//import org.akkatrading.live.util.{InstrumentEnum, TimeFrames}
//import org.scalamock.scalatest.MockFactory
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
//import org.akkatrading.live.AccountFetcher.AccountResponse
//
//class TradingStateTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
//  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory with TestUtils {
//
//  "FetchTradeResponse event" should {
//    val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//    "stay in Trading" in {
//      fsm.setState(Trading, StrategyState(List(), new TimeFrames(Map()), new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0))
//      val trades = List(tradeRandom(), tradeRandom())
//      fsm ! FetchTradeResponse(trades)
//      fsm.stateName should be (Trading)
//      fsm.stateData.asInstanceOf[StrategyState].orders should be (trades)
//    }
//  }
//
//}
