//package org.akkatrading.live
//
//import akka.actor.ActorSystem
//import akka.testkit.{ImplicitSender, TestFSMRef, TestKit, TestProbe}
//import org.akkatrading.live.CandleFetcher.{FetchCandles, PriceUpdate}
//import org.akkatrading.live.OrderFetcher.{FetchOrderRequest, FetchTradeResponse}
//import org.akkatrading.live.StrategyFSM.{BeforeStart, DownloadingMissingCandles, StrategyState, Trading}
//import org.akkatrading.live.strategy.Strategy
//import org.akkatrading.live.util.{InstrumentEnum, TimeFrameEnum, TimeFrames}
//import org.scalamock.scalatest.MockFactory
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
//import org.akkatrading.live.AccountFetcher.AccountResponse
//
//class BeforeStartStateTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
//  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory with TestUtils {
//
//  "PriceUpdate event" should {
//    val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//    "be ignored for different instrument" in {
//      fsm ! PriceUpdate(InstrumentEnum.EUR_CAD, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      fsm.stateName should be (BeforeStart)
//    }
//    "move to DownloadingMissingCandles if not enough candles" in {
//      fsm ! PriceUpdate(InstrumentEnum.EUR_USD, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      fsm.stateName should be (DownloadingMissingCandles)
//    }
//    "transition to Trading if enough candles" in {
//      class TimeFramesStub extends TimeFrames(null)
//      val timeFrames = stub[TimeFramesStub]
//      timeFrames.handlePriceUpdate _ when * returning timeFrames
//      timeFrames.enoughCandles _ when * returning true
//      fsm.setState(BeforeStart, StrategyState(List(), timeFrames, new AccountResponse(0.0, "", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ""), 1.0))
//      fsm ! PriceUpdate(InstrumentEnum.EUR_USD, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      fsm.stateName should be (Trading)
//    }
//  }
//
//  "Communication: PriceUpdate event" should {
//    "send FetchCandles to candleFetcher if not enough candles" in {
//      val candleFetcher = TestProbe()
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref, TestProbe().ref, TestProbe().ref, candleFetcher.ref,  stub[Strategy], false))
//      fsm ! PriceUpdate(InstrumentEnum.EUR_USD, TimeFrameEnum.M1, candleRandom(), candleRandom())
//      candleFetcher.expectMsg(FetchCandles(InstrumentEnum.EUR_USD, 50, TimeFrameEnum.M1, "bidask"))
//    }
//  }
//
//  "Trades response" should {
//    "stay in BeforeStart state" in {
//      val fsm = TestFSMRef(new StrategyFSM(TestProbe().ref, InstrumentEnum.EUR_USD, TestProbe().ref,  TestProbe().ref, TestProbe().ref, TestProbe().ref, stub[Strategy], false))
//      fsm ! FetchTradeResponse(List())
//      fsm.stateName should be (BeforeStart)
//    }
//  }
//}
