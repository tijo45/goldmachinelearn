packageArchetype.java_application

organization := "org.akkatrading"

name := "akka-trading"

version := "1.0"

scalaVersion := "2.12.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

updateOptions := updateOptions.value.withCachedResolution(true)

resolvers += Resolver.bintrayRepo("cibotech", "public")

libraryDependencies ++= {
  val akkaVersion = "2.5.22"
  Seq(
    "org.specs2" %% "specs2-core" % "4.5.1" % Test,
    "org.scalatest" %% "scalatest" % "3.0.5" % "test",
    "com.softwaremill" %% "helisa" % "0.8.0",
    "com.msilb" %% "scalanda-v20" % "0.1.5",
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "ch.qos.logback" % "logback-classic" % "1.1.2",
    "com.typesafe.akka" %% "akka-http"   % "10.1.8",
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    "com.cibo" %% "evilplot" % "0.6.3"
  )
}

Revolver.settings
